#pragma once

#include "backend/backend.h"
#include <memory>
#include <string>

namespace pxwm {
    namespace ipc {
        class Server {
        public:
            bool server_running;

            Server(std::string socketfile, std::shared_ptr<pxwm::backend::Backend> be_inst);
            Server(const Server&) = delete;
            Server& operator=(const Server&) = delete;
            ~Server();

        private:
            class Priv;
            class MsgHandlers;
            std::unique_ptr<Priv> p;
        };
    }
}
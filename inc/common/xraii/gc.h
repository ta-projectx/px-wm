#pragma once

#include <xcb/xcb.h>

namespace pxwm {
    namespace common {
        namespace xraii {
            /**
             * @brief RAII wrapped version of XCB's graphics context.
             * This object is meant to be used with a smart pointer, and therefore cannot be moved or copied.
             */
            class GC {
            public:
                xcb_connection_t* const conn;
                const xcb_gcontext_t id;

                GC();
                GC(xcb_connection_t* conn, xcb_drawable_t drawable, uint32_t value_mask, const uint32_t* value_list);
                GC(const GC&) = delete;
                GC& operator=(const GC&) = delete;
                ~GC();

            private:
                xcb_gcontext_t init(xcb_connection_t* conn,
                    xcb_drawable_t drawable,
                    uint32_t value_mask,
                    const uint32_t* value_list);
            };
        }
    }
}
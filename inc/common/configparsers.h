#pragma once

#include "config.h"
#include "shared/fixedpt.h"
#include <map>

namespace pxwm {
    namespace common {
        namespace configparsers {
            namespace commonparsers {
                bool parse_bool(const char* arg, bool& out);

                template<typename T>
                bool parse_class_enum(const char* arg, const std::map<std::string, T>& str_map, T& out) {
                    auto ret = str_map.find(arg);
                    if (ret == str_map.end()) {
                        return false;
                    }
                    out = ret->second;
                    return true;
                }
            }

            class BkFrontEndOptionsParser {
            public:
                static bool parse_default_scale_factor(const char* arg, uint16_t& out);
                static bool parse_scale_algorithm(const char* arg, pxwm::states::ScalingAlgorithm& out);
                static bool parse_framerate_mode(const char* arg, pxwm::states::FrameRateMode& out);
                static bool parse_shm_mode(const char* arg, pxwm::states::SHMMode& out);
            };

            class OptionsValidator {
            public:
                static bool check_scaler_scale(pxwm::states::ScalingAlgorithm alg, uint8_t scfactor);
            };
        }
    }
}
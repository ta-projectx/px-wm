#pragma once

#include "frontend/widtrans.h"
#include "intermsg/frontend_eventloop.h"
#include "sharedds/cursordata.h"
#include "sharedds/windowproperties.h"
#include <map>
#include <memory>
#include <queue>
#include <set>
#include <xcb/xproto.h>

namespace pxwm {
    namespace common {
        namespace handlerhelpers {
            class FocusIOHelper {
            public:
                /**
                 * @param fe_focused_state A reference to a pair of focused window state in frontend. This helper's
                 * instance will modify the contents of this variable if necessary.
                 * @param be_msg_queue A reference to backend message queue, used by this helper to send commands
                 * regarding focus change to backend.
                 * @param wid_trans A pointer to a window translator. Used by this helper to translate frontend's window
                 * ID to backend's window ID, and vice versa.
                 * @param fconn A pointer to the frontend's XCB connection.
                 */
                FocusIOHelper(std::pair<xcb_window_t, xcb_window_t>& fe_focused_state,
                    std::queue<intermsg::frontendeventloop::message_store_t>& be_msg_queue,
                    frontend::WIDTrans* wid_trans,
                    xcb_connection_t* fconn);
                ~FocusIOHelper();
                FocusIOHelper(const FocusIOHelper&) = delete;
                FocusIOHelper& operator=(const FocusIOHelper&) = delete;

                bool focus_changed = false;

                void forward_be();

            private:
                class Priv;
                std::unique_ptr<Priv> p;
            };

            class ConfigureNotifyHelper {
            public:
                // flag and data pair (fd)
                typedef std::pair<intermsg::frontendeventloop::GeomCfgUpdated, sharedds::WindowGeomProperties>
                    fd_geom_t;
                void insert(xcb_window_t id, fd_geom_t& new_data);
                std::map<xcb_window_t, fd_geom_t>& get();
                void clear();

            private:
                std::map<xcb_window_t, fd_geom_t> store;
            };

            class CursorHelper {
            public:
                CursorHelper(xcb_connection_t* fconn, xcb_screen_t* fscr);
                ~CursorHelper();
                void change_cursor(sharedds::CursorData cur);
                void assign_to_window(xcb_window_t fwinid);

            private:
                class Priv;
                std::unique_ptr<Priv> p;
            };

            class BEShapeNotifyHelper {
            public:
                std::set<xcb_window_t> update_list;
            };
        }
    }
}
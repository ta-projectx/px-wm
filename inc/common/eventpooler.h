#pragma once

#include <functional>
#include <map>
#include <memory>
#include <xcb/xcb.h>

namespace pxwm {
    namespace common {
        namespace x11 {
            class EventPooler {
            public:
                /**
                 * Construct EventPooler.
                 * @param conn The underlying XCB connection
                 * @param event_handlers A reference to map of event handler functions. The functions from the map will
                 * be copied internally. The function's key must correspond to their respective supposed response type.
                 * The event handler function must accept three parameters: pointer to xcb_generic_event_t, an int
                 * (count that kind of event occurrence in current queue) and a bool (indicates whether that kind of
                 * event is the last occurrence in this queue).
                 */
                EventPooler(xcb_connection_t* conn,
                    std::map<uint8_t, std::function<void(xcb_generic_event_t*, int, bool)>>& event_handlers);
                EventPooler(EventPooler&) = delete;
                ~EventPooler();
                EventPooler& operator=(const EventPooler&) = delete;
                /**
                 * Pool for event(s) until the current queue is empty, and call the associated handler (if exists).
                 * @return false if the underlying X connection is dropped, true otherwise
                 */
                bool poll_for_events();
                /**
                 * @brief Pool for a single event.
                 * If the handler returns false, the event will be requeued for use by the regular event pooler (e.g.
                 * the one that called pool_for_events), otherwise the event is assumed to be handled and won't be
                 * requeued.
                 * @param handler The one-time event handler function to use.
                 * @param timeout The maximum ammount of timeout to wait.
                 */
                void custom_wait_for_event(std::function<bool(const xcb_generic_event_t*)> handler, timespec timeout);

            private:
                class Priv;
                std::unique_ptr<Priv> p;
            };
        }
    }
}

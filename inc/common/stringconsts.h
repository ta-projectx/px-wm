#pragma once

#include "shared/i18n.h"

namespace pxwm {
    namespace stringconsts {
        // Put the commonly used string consts here. Do not gettext strings retrieved
        // from here.
        struct StringConstsCont {
            const char* PLEASE_READ_HELP = _("Read '--help' for usage details.");
            const char* SOCKET_INCOMPLETE_WRITE_CLIENT =
                _("Incomplete socket write. Command may not be recognized by server.");
            const char* INVALID_RESPONSE = _("Invalid response.");
            const char* EMPTY_RESPONSE = _("Empty response.");
            const char* RESPONSE_TRUNCATED = _("Response truncated. Data maybe invalid.");
            const char* ERROR_PARSE_ARG = _("Error occured when parsing arguments.");
            const char* ERROR_XRENDER_FMT_NOT_AVAIL = _("Cannot select suitable RENDER format for %1$s.");
            const char* WARN_POINTER_FWD = _("Mouse pointer won't be forwarded.");
        };

        StringConstsCont& values();
    }
}
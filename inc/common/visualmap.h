#pragma once

#include <memory>
#include <unordered_set>
#include <vector>
#include <xcb/xcb.h>

namespace pxwm {
    namespace common {
        namespace x11 {
            struct VisualInfo {
                xcb_visualid_t id;
                // values from xcb_visual_class_t enum
                uint8_t visual_class;
                uint8_t depth;
                uint8_t bits_per_rgb_value;
            };

            class VisualMap {
            public:
                VisualMap(xcb_connection_t* conn, xcb_screen_t* scr);
                VisualMap(const VisualMap&) = delete;
                VisualMap& operator=(const VisualMap&) = delete;
                ~VisualMap();
                const std::vector<VisualInfo>& available_visuals();
                /**
                 * Retrieve the visual info from a visual ID.
                 * @return The visual info. If ID is not found, the id member of the return value is zero.
                 */
                const VisualInfo get_visual_type(xcb_visualid_t id);
                /**
                 * @brief Retrieve a visual info from the specified depth.
                 * This method will look for a visual whose depth matches the specification, and bits per channel set to
                 * 8, and class is either TrueColor or DirectColor.
                 * @return The visual info. If not found, the id member of the return value is zero.
                 */
                const VisualInfo get_visual_type_by_depth(uint8_t depth);
                /**
                 * Get the visual info from the default visual of the screen that was specified on the construction of
                 * this instance.
                 */
                const VisualInfo default_visual();
                xcb_colormap_t get_default_visual_colormap(xcb_visualid_t visualid);
                const std::unordered_set<uint8_t>& get_pixmap_depth_bpp_format(uint8_t depth);
                const std::unordered_set<uint8_t>& get_pixmap_scanline_pad(uint8_t depth);

            private:
                class Priv;
                std::unique_ptr<Priv> p;
            };
        }
    }
}
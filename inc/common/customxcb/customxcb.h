#pragma once

#include "3rdparty/xcbxkb_compat.h"
#include <xcb/xcbext.h>

/**
 * @brief Re-implements some XCB requests that are required by this app.
 * Some XCB implementations, especially on some extensions are broken in several ways. If those functions in the broken
 * implementation is used, it will either causes unintended effect to the server, or results in a request that always
 * errors
 * regardless of the correctness of the data in the request.
 */

namespace pxwm {
    namespace common {
        namespace customxcb {
            /**
             * @brief Request to server to lock and/or latch it's modifiers and/or groups.
             * The original is broken because it doesn't accept and send modLatches.
             * @return A void cookie. Must be checked for error with xcb_request_check.
             */
            xcb_void_cookie_t xcb_custom_xkb_latch_lock_state_checked(xcb_connection_t* c,
                xcb_xkb_device_spec_t deviceSpec,
                uint8_t affectModLocks,
                uint8_t modLocks,
                uint8_t lockGroup,
                uint8_t groupLock,
                uint8_t affectModLatches,
                uint8_t modLatches,
                uint8_t latchGroup,
                uint16_t groupLatch);

            /**
             * @brief Serialize xcb_xkb_set_map_values_t into a buffer.
             * The original is broken because of wrong padding value and some key types data doesn't get serialized.
             * @param _buffer The returned buffer. Cannot be NULL. Must be free-ed by caller.
             * @return Size of the returned buffer.
             */
            int xcb_custom_xkb_set_map_values_serialize(void** _buffer,
                uint8_t nTypes,
                uint8_t nKeySyms,
                uint8_t nKeyActions,
                uint16_t totalActions,
                uint8_t totalKeyBehaviors,
                uint16_t virtualMods,
                uint8_t totalKeyExplicit,
                uint8_t totalModMapKeys,
                uint8_t totalVModMapKeys,
                uint16_t present,
                const xcb_xkb_set_map_values_t* _aux);

            /**
             * @brief Set server's XKB map from the provided values.
             * The original is broken because it doesn't calculates the iovec request size correctly.
             * @return A void cookie. Must be checked for error with xcb_request_check.
             */
            xcb_void_cookie_t xcb_custom_xkb_set_map_checked(xcb_connection_t* c,
                xcb_xkb_set_map_request_t& xcb_out,
                const void* values,
                int vallen);

            /**
             * @brief Serialize xcb_xkb_set_names_values_t into a buffer.
             * The original is broken because of wrong padding value and some key types data doesn't get serialized.
             * @param _buffer The returned buffer. Cannot be NULL. Must be free-ed by caller.
             * @return Size of the returned buffer.
             */
            int xcb_custom_xkb_set_names_values_serialize(void** _buffer,
                uint8_t nTypes,
                uint8_t nKTLevels,
                uint32_t indicators,
                uint16_t virtualMods,
                uint8_t groupNames,
                uint8_t nKeys,
                uint8_t nKeyAliases,
                uint8_t nRadioGroups,
                uint32_t which,
                const xcb_xkb_set_names_values_t* _aux);

            /**
             * @brief Set server's XKB names from the provided values.
             * The original is broken because it doesn't calculates the iovec request size correctly.
             * @return A void cookie. Must be checked for error with xcb_request_check.
             */
            xcb_void_cookie_t xcb_custom_xkb_set_names_checked(xcb_connection_t* c,
                xcb_xkb_device_spec_t deviceSpec,
                uint16_t virtualMods,
                uint32_t which,
                uint8_t firstType,
                uint8_t nTypes,
                uint8_t firstKTLevelt,
                uint8_t nKTLevels,
                uint32_t indicators,
                uint8_t groupNames,
                uint8_t nRadioGroups,
                xcb_keycode_t firstKey,
                uint8_t nKeys,
                uint8_t nKeyAliases,
                uint16_t totalKTLevelNames,
                const void* values,
                int vallen);

            /**
             * Wait the XCB's event queue for events with maximum wait time for given interval.
             * @param timeout Maximum wait time.
             * @return A pointer to an XCB event, or NULL if timed out.
             */
            xcb_generic_event_t* xcb_timed_wait_for_event(xcb_connection_t* c, timespec timeout);
        }
    }
}
#pragma once

#include "common/eventpooler.h"
#include "sharedds/selection.h"
#include "sharedds/windowproperties.h"
#include <memory>
#include <string>
#include <tuple>
#include <vector>
#include <xcb/xcb.h>
#include <xcb/xfixes.h>

namespace pxwm {
    namespace common {
        namespace x11 {
            /**
             * @brief Manages selection redirections.
             * This class is not designed to be thread safe. Both backend and frontend should serialize the call before
             * calling the methods in this class.
             */
            class SelMgr {
            public:
                const xcb_window_t proxy_wid;
                /**
                 * @brief Construct an instance of selection manager.
                 * An unmapped window will be created on the specified display, whose job is to listen to seelction
                 * owner changes (via xfixes) and act as a selection owner's proxy. Any events occured in this class'
                 * window must be handled by the display's event loop.
                 * @param conn The display to manage.
                 */
                SelMgr(xcb_connection_t* conn, xcb_screen_t* scr);
                SelMgr(const SelMgr&) = delete;
                SelMgr& operator=(const SelMgr&) = delete;
                ~SelMgr();
                /**
                 * Check if this instance is usable. If it isn't, any method on this instance must not be called, or
                 * undefined behaviour may occur.
                 */
                bool is_usable();
                /**
                 * Get the list of selections managed by this selection manager.
                 */
                const std::vector<xcb_atom_t> managed_selections();
                /**
                 * @brief Clear the unhandled selection request data for the specified window.
                 * This should be called on window destroy event, to free up the unused memory.
                 */
                void destroy_requestor_data(xcb_window_t wid);
                /**
                 * Acquire the specified selection for the specified display. An unrecognized selection will be silently
                 * ignored.
                 */
                void acquire_selection(xcb_atom_t sel);
                /**
                 * Release the specified selection for the specified display. If this instance doesn't already own the
                 * specified selection, the selection owner won't change. An unrecognized selection will be silently
                 * ignored.
                 */
                void release_selection(xcb_atom_t sel);
                /**
                 * @brief Notify this manager that an application in the same display as this manager is requesting the
                 * selection value.
                 * This method will generate zero or more properties, depending on the request. If the returned list has
                 * zero elements, then the requestor is guaranteed to be sent an empty SelectionNotify event. This
                 * method will also assign the StructureNotify event mask to the specified conn if it is not listened
                 * yet.
                 * @param cevt The selection request event object.
                 * @return The requested types along with their target properties to be forwarded in the redirected
                 * display's window.
                 */
                std::pair<sharedds::SelectionInfo, std::vector<sharedds::SelectionRequest>> handle_request(
                    xcb_selection_request_event_t* cevt);
                /**
                 * @brief Supply the replied selection data to the window that requested the selection value.
                 * All requested properties must be given at once. A SelectionNotify will be sent, along with the
                 * supplied data after this method is called.
                 * @param requestor The window that has previously requested selection data.
                 * @param sel The selection atom in which the request take place, and it's associated property. Value of
                 * NONE in both properties atom means that the owner rejected the request.
                 * @param properties List of supplied data. The properties ID must match the one returned from
                 * handle_request.
                 */
                void supply_data(common::x11::EventPooler* eventpooler,
                    xcb_window_t requestor,
                    sharedds::SelectionInfo sel,
                    std::vector<sharedds::XProperty>& properties);
                /**
                 * @brief Request the owner for selection data.
                 * This method will use this instance's proxy window for as the requestor. All properties will be
                 * translated to unique name based on the arguments given, which contains the requestor_id. Make sure
                 * that requestor_id is the same for each requestor, and must be different between different requestor.
                 * @param id The unique ID for the requestor.
                 * @return false if error occurs, true if succeed.
                 */
                bool request_selection(uint32_t id,
                    sharedds::SelectionInfo selinfo,
                    std::vector<sharedds::SelectionRequest>& preqs);
                /**
                 * Parse a selection notify events directed to proxy_wid, and return it's info and values.
                 * @param eventpooler The display's EventPooler object, used for receiving INCR replies.
                 * @param cevt The event that was sent from the selection owner.
                 * @return A tuple that consists of requestor ID, selection information and retrieved property values.
                 */
                std::tuple<uint32_t, sharedds::SelectionInfo, std::vector<sharedds::XProperty>> parse_selection_notify(
                    common::x11::EventPooler* eventpooler,
                    xcb_selection_notify_event_t* cevt);

            private:
                class Priv;
                std::unique_ptr<Priv> p;
            };

            class SelConverter {
            public:
                /**
                 * Convert all atoms in the specified requests. Also convert property values whose type is ATOM.
                 */
                static void convert_request_atoms(xcb_connection_t* src_conn,
                    xcb_connection_t* dst_conn,
                    sharedds::SelectionInfo& selinfo,
                    std::vector<sharedds::SelectionRequest>& reqs);
                /**
                 * Convert all atoms in the specified reply data. Also convert property values whose type is ATOM.
                 */
                static void convert_reply_atoms(xcb_connection_t* src_conn,
                    xcb_connection_t* dst_conn,
                    sharedds::SelectionInfo& selinfo,
                    std::vector<sharedds::XProperty>& reps);
            };
        }
    }
}
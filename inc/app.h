#pragma once

#include "config.h"
#include <memory>

namespace pxwm {
    constexpr int VERSION_ORD = 26;

    class MainApp {
    public:
        MainApp();
        MainApp(const MainApp&) = delete;
        MainApp& operator=(const MainApp&) = delete;
        MainApp(MainApp&&);
        ~MainApp();
        void run(int argc, char** argv);
        std::shared_ptr<states::Config> cfg;

    private:
        class Priv;
        std::unique_ptr<Priv> p;
    };
}
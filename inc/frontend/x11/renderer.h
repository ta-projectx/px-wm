#pragma once

#include "common/visualmap.h"
#include "config.h"
#include "sharedds/windowupdatedata.h"
#include <memory>
#include <xcb/render.h>
#include <xcb/xcb.h>

namespace pxwm {
    namespace frontend {
        namespace x11 {
            /**
             * This class is meant to be paired with X11Frontend.
             */
            class Renderer {
            public:
                Renderer(std::string display_name,
                    xcb_connection_t* be_conn,
                    xcb_connection_t* fe_conn,
                    xcb_screen_t* fe_scr,
                    common::x11::VisualMap* be_visualmap,
                    common::x11::VisualMap* fe_visualmap,
                    std::map<uint8_t, xcb_render_pictforminfo_t> render_formats,
                    states::BkFrontEndOptions options);
                ~Renderer();
                Renderer(const Renderer&) = delete;
                Renderer& operator=(const Renderer&) = delete;
                bool init_has_error();
                /**
                 * Create a new window in renderer. This method should be called by frontend on new_window.
                 * @param be_depth Backend window visual's depth. This is assumed to be either 24 or 32. No checking
                 * performed.
                 * @param fe_depth Frontend window visual's depth. This is assumed to be either 24 or 32. No checking
                 * performed.
                 */
                void new_window(xcb_window_t fwid,
                    xcb_window_t bwid,
                    std::pair<uint16_t, uint16_t> size,
                    uint8_t be_depth,
                    uint8_t fe_depth);
                /**
                 * Call after window become unmanaged to free it's resources inside this instance.
                 */
                void delete_window(xcb_window_t fwid);
                /**
                 * Call to retrieve backend window's content and redraw the frontend window.
                 * @param size_changed true to indicate if a size change is presumed to occur. A full redraw will be
                 * performed and size will be updated using area parameter, ignoring offset.7
                 * @param area The updated area.
                 */
                void update_window(xcb_window_t fwid, bool size_changed, xcb_rectangle_t area);
                /**
                 * Call on scale factor change.
                 */
                void rescale(uint16_t scfactor);

            private:
                class Priv;
                std::unique_ptr<Priv> p;
            };
        }
    }
}
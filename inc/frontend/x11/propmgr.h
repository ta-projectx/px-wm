#pragma once

#include "frontend/widtrans.h"
#include "sharedds/windowproperties.h"
#include <memory>
#include <vector>
#include <xcb/xcb.h>

namespace pxwm {
    namespace frontend {
        namespace x11 {
            struct PropData {
                xcb_window_t target;
                xcb_atom_t property;
                // use only values from xcb_property_t
                uint8_t state;
                sharedds::XPropertyValue prop_val;
            };

            class PropMgr {
            public:
                PropMgr(WIDTrans* wid_trans, xcb_connection_t* fconn, xcb_connection_t* bconn);
                ~PropMgr();
                PropMgr(const PropMgr&) = delete;
                PropMgr& operator=(const PropMgr&) = delete;
                /**
                 * Modify backend's received property update so that it is suitable for use in frontend.
                 * @param prop Reference to property data to modify. Guaranteed to be intact if return is false. Set
                 * target and atom members to backend's.
                 * @return true if this property should be forwarded to frontend, false otherwise.
                 */
                bool forward_property(PropData& prop);
                /**
                 * Modify frontend's received property update so that it is suitable for use in backend.
                 * @param prop Reference to property data to modify. Guaranteed to be intact if return is false. Set
                 * target and atom members to frontend's.
                 * @return true if this property should be forwarded to backend, false otherwise.
                 */
                bool backward_property(PropData& prop);
                /**
                 * Free any additional properties data/resources associated with a window.
                 * @param wid Frontend window ID to delete.
                 */
                void delete_frontend_window(xcb_window_t wid);
                /**
                 * @brief Set the scaling factor, so that any size based properties get scaled.
                 * @param scfactor The value of scaling factor configuration.
                 * @return List of PropData to be reforwarded.
                 */
                std::vector<PropData> set_fe_scaling_factor(uint16_t scale_factor);

                /**
                 * Get the complete property info and value from a property notify event.
                 * @param conn X connection where evt came.
                 * @param evt Event object to get info from.
                 * @return Property data. If any error occurred, return's target will be set to 0.
                 */
                static PropData convert_from_event(xcb_connection_t* conn, xcb_property_notify_event_t* evt);
                static PropData convert_from_xproperty(xcb_window_t target, sharedds::XProperty const& prop);
                static PropData get_from_window(xcb_connection_t* conn, xcb_window_t window, xcb_atom_t prop);

            private:
                class Priv;
                std::unique_ptr<Priv> p;
            };
        }
    }
}
#pragma once

#include "3rdparty/cameron314/readerwriterqueue.h"
#include "common/visualmap.h"
#include "common/x11.h"
#include "config.h"
#include "intermsg/eventloop_backend.h"
#include <condition_variable>
#include <cstdint>
#include <map>
#include <memory>
#include <string>
#include <vector>
#include <xcb/xcb.h>

namespace pxwm {
    namespace backend {
        namespace eventloop {
            /*
             * Contains event looper, and handles any event that happens
             */
            class EventLoop {
            public:
                EventLoop(xcb_connection_t* conn,
                    xcb_screen_t* scr,
                    std::string display_string,
                    std::map<xcb_extension_t*, pxwm::common::x11::XExtensionInfo> ext_first_events,
                    std::shared_ptr<pxwm::common::x11::VisualMap> visualmap,
                    pxwm::states::BkFrontEndOptions options);
                EventLoop(const EventLoop&) = delete;
                EventLoop& operator=(const EventLoop&) = delete;
                ~EventLoop();

                moodycamel::ReaderWriterQueue<pxwm::intermsg::eventloopbackend::Message> msg_queue;
                const std::string display_string;
                std::atomic<bool> has_queue{false};

                /**
                 * Start the EventLoop alongside a frontend in a separate thread.
                 * @param p_cv A condition variable shared with main thread to notify if this EventLoop thread has some
                 * message to tell to the main thread (e.g. stopped).
                 * @param initial_windows A list of xcb_window_t that will be displayed at frontend. The contents of the
                 * vector will be copied internally upon call to this function.
                 * @param frontend_display Display which the frontend should connect to.
                 */
                void start_run_loop(std::shared_ptr<std::condition_variable> p_cv,
                    std::vector<xcb_window_t>& initial_windows);
                void request_stop();
                void request_rescale(uint16_t scfactor);
                /**
                 * Returns list of currently managed windows by this eventloop and it's frontend.
                 * @return A vector of tuple of xcb_window_t, where the first of the tuple is backend window ID, and the
                 * second of the tuple is frontend window ID.
                 */
                std::vector<std::pair<xcb_window_t, xcb_window_t>> get_managed_windows();
                pxwm::states::BkFrontEndOptions get_applied_options();

            private:
                class Priv;
                class Handlers;
                class FrontendHandlers;
                std::unique_ptr<Priv> p;
            };
        }
    }
}
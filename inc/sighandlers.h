#pragma once

#include "backend/backend.h"
#include <memory>

namespace pxwm {
    namespace sighandlers {
        void install_signal_handlers(std::shared_ptr<pxwm::backend::Backend> p_backend_inst);
        bool is_sighandler_running();
    }
}
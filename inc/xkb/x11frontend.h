#pragma once

#include "sharedds/xkb.h"
#include "xkb/shared.h"
#include <memory>
#include <xcb/xcb.h>

namespace pxwm {
    namespace xkb {
        class X11Frontend {
        public:
            /**
             * @brief Construct the instance.
             * @param fconn A connected and working xcb connection to frontend.
             * @param bconn A connected and working xcb connection to backend (for retreiving atoms, etc.).
             */
            X11Frontend(xcb_connection_t* fconn, xcb_connection_t* bconn);
            X11Frontend(const X11Frontend&) = delete;
            X11Frontend& operator=(const X11Frontend&) = delete;
            ~X11Frontend();
            bool get_latch_lock_state(shared::FELatchLockStateReply& ret);
            bool get_controls(shared::FEControlsReply& ret);
            bool forward_desc_to_backend(sharedds::XkbDescUpdateFlag flag);

        private:
            class Priv;
            std::unique_ptr<Priv> p;
        };
    }
}
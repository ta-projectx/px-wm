#pragma once

/**
 * @brief Shared data structure for communicating frontend's XKB reply with backend.
 * Every data structure here is based on X11 XKB's flags. Regardless of the frontend, every data must be converted to
 * XKB flags first.
 */

#include <cstdint>

namespace pxwm {
    namespace xkb {
        namespace shared {
            struct FELatchLockStateReply {
                uint8_t latched_mods;
                uint8_t locked_mods;
                uint8_t latched_group;
                uint8_t locked_group;
            };

            struct FEControlsReply {
                /**
                 * Only REPEAT_KEY and STICKY_KEYS will be checked.
                 */
                uint32_t enabled_controls;
                /**
                 * If STICKY_KEYS is enabled, only LATCH_TO_LOCK and TWO_KEYS will be checked.
                 */
                uint16_t sk_ax_options;
                uint16_t rk_repeat_delay;
                uint16_t rk_repeat_interval;
            };
        }
    }
}
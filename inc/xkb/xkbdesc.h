#pragma once

/**
 * @brief Collection of functions to forward XKB descs from an X display to another X display.
 * XKB descs that can be forwarded are maps, compat maps and names. All of these functions uses XCB API to do their
 * jobs.
 */

#include "3rdparty/xcbxkb_compat.h"
#include <xcb/xcb.h>

namespace pxwm {
    namespace xkb {
        namespace desc {
            bool forward_xkb_map(xcb_connection_t* src_conn, xcb_connection_t* dst_conn, xcb_xkb_get_map_reply_t** out);
            bool forward_xkb_compat_map(xcb_connection_t* src_conn, xcb_connection_t* dst_conn);
            bool forward_xkb_names(xcb_connection_t* fconn, xcb_connection_t* bconn, int firstType);
            bool convert_xkb_names_atoms(xcb_connection_t* src_conn,
                xcb_connection_t* dst_conn,
                xcb_xkb_get_names_reply_t& src_rep,
                xcb_xkb_set_names_values_t& dst_data);
        }
    }
}
#pragma once

/**
 * This header fixes XCB's XKB header compatibility with C++11.
 */

#define explicit explicit1
#include <xcb/xkb.h>
#undef explicit
#pragma once

#include <cstdint>
#include <xcb/xcb.h>

namespace pxwm {
    namespace sharedds {
        struct ClientMsg {
            uint8_t format;
            xcb_window_t window;
            xcb_atom_t msg_type;
            struct {
                uint8_t propagate;
                uint32_t event_mask;
                xcb_window_t destination;
            } send_info;
            xcb_client_message_data_t data;
        };
    }
}
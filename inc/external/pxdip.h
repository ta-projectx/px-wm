#ifndef PXDIP_H
#define PXDIP_H

#define PXDIP_API 2

#include <cstdint>

extern "C" {
/**
 * @brief Get the API version of this library.
 * This version will be incremented only if there is a modification and/or addition of one of more functions in this
 * header file, which may or may not affect backward compatibility.
 */
unsigned int pxdip_api_version();
/**
 * Get the revision number of this library.
 */
unsigned int pxdip_revision();
/**
 * Auxiliary string that describes this library.
 * @return null terminated UTF-8 string.
 */
const char* pxdip_version_desc();
/**
 * Creates a new instance of DIP library.
 * @param max_thread Maximum number of threads that will be used to process data by this instance. Set to zero for the
 * library to automatically determine max no. of threads.
 * @return Opaque pointer to the instance of this library.
 */
void* pxdip_init(const uint8_t max_thread);
/**
 * Destroy an instance of a DIP library.
 * @param inst Opaque pointer to an existing instance.
 */
void pxdip_destroy(const void* inst);
/**
 * @brief Sets each value in data with mask.
 * Operation: data[i] = data[i] OR mask. This will replace all values in data.
 * @param count Number of items in data.
 * @param data Pointer to the data to be modified.
 */
void pxdip_setmask(const void* inst, const uint32_t count, uint32_t* const data, const uint32_t mask);

/**
 * @brief Perform a copy from a rectangle region from image src to dst.
 * The area will be enlarged with 2x scale factor using RAA algorithm, and copied to dst. dst's must be exactly twice
 * the size of src in both axis.
 * @param cp_x Left offset of rectangle area from src to be copied.
 * @param cp_y Top offset of rectangle area from src to be copied.
 * @param cp_w Width of rectangle area from src to be copied.
 * @param cp_h Height of rectangle area from src to be copied.
 * @param src_w Source image's total width.
 * @param src_h Source image's total height.
 * @param src Pointer to source image.
 * @param dst Pointer to destination image.
 */
void pxdip_raa(const void* inst,
    const uint16_t cp_x,
    const uint16_t cp_y,
    const uint16_t cp_w,
    const uint16_t cp_h,
    const uint16_t src_w,
    const uint16_t src_h,
    uint32_t* const src,
    uint32_t* const dst);
}

#endif
Welcome to Codename Project X!
=======================

These collective projects, whose collective codename is "Project X", is intended to enable individual window bitmap scaling on operating systems based on Linux kernel and utilizing X window system.

The main goal of this project is to make sure that graphical applications utilizing legacy GUI toolkits to be properly displayed on high-density screens, without layout distortion, by utilizing bitmap scaling on those applications' windows. This method has an expense that the scaled applications will look pixelated compared to natively scaled applications.

More info coming soon! Stay tuned!

License
-------

Copyright (c) 2018 Codename Project X authors and contributors.

All files in the Codename Project X repositories, including those files that exists in commits and branches without this README file, are licensed under GPLv3, except on the repository `px-launcher` for the target named `px_preload`, which is licensed under LGPLv3. A copy of GPLv3 license can be found at COPYING file in all Codename Project X repositories, and a copy of LGPLv3 license can be found at COPYING.LESSER file in the `px-launcher` repository.

If a file provides a copyright notice and/or license information, then the copyright and license information in that particular file takes precedence over the license and copyright information in this README file.

If a newer README file conflicts with the older README file, the new file takes the precedence over the old file.

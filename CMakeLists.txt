cmake_minimum_required(VERSION 3.4)
project(px_wm)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_SKIP_RPATH TRUE)

add_subdirectory(deps/px-shared)
include_directories(inc deps/px-shared/inc)

set(PX_WM_SOURCE_FILES src/main.cpp inc/app.h src/app.cpp inc/config.h src/config.cpp inc/backend/backend.h src/backend/backend.cpp inc/common/x11.h src/common/x11.cpp inc/backend/common.h src/backend/common.cpp inc/backend/eventloop.h src/backend/eventloop.cpp inc/3rdparty/cameron314/concurrentqueue.h inc/intermsg/eventloop_backend.h inc/3rdparty/cameron314/readerwriterqueue.h inc/3rdparty/cameron314/atomicops.h inc/ipc/client.h inc/ipc/server.h src/ipc/client.cpp src/ipc/server.cpp inc/frontend/x11.h src/frontend/x11.cpp inc/sharedds/windowupdatedata.h inc/sighandlers.h src/sighandlers.cpp inc/common/configparsers.h src/common/configparsers.cpp inc/intermsg/frontend_eventloop.h inc/common/eventpooler.h inc/sharedds/windowproperties.h inc/common/handlerhelpers.h src/common/handlerhelpers.cpp src/common/eventpooler.cpp inc/3rdparty/xcbxkb_compat.h inc/xkb/backend.h inc/xkb/x11frontend.h inc/sharedds/xkb.h src/xkb/backend.cpp inc/xkb/shared.h src/xkb/x11frontend.cpp inc/xkb/xkbdesc.h src/xkb/xkbdesc.cpp inc/common/customxcb/customxcb.h src/common/customxcb/xkb.cpp inc/frontend/widtrans.h inc/frontend/x11/propmgr.h src/frontend/x11/propmgr.cpp inc/frontend/x11/cmmgr.h inc/sharedds/clientmsgdata.h src/frontend/x11/cmmgr.cpp inc/common/xraii/gc.h src/common/xraii/gc.cpp inc/common/xraii/pixmap.h src/common/xraii/pixmap.cpp inc/common/xraii/xrenderpicture.h src/common/xraii/xrenderpicture.cpp inc/common/stringconsts.h inc/ipc/consts.h src/ipc/consts.cpp src/common/stringconsts.cpp inc/sharedds/cursordata.h inc/common/selmgr.h src/common/selmgr.cpp src/common/customxcb/misc.cpp inc/sharedds/selection.h inc/frontend/x11/renderer.h src/frontend/x11/renderer.cpp inc/common/visualmap.h src/common/visualmap.cpp inc/common/xraii/shm.h src/common/xraii/shm.cpp inc/common/xraii/colormap.h src/common/xraii/colormap.cpp inc/external/pxdip.h)
set(PX_WM_DEPENDENCIES px_shared)

#activate pkgconfig
find_package(PkgConfig REQUIRED)

pkg_check_modules(XCB xcb)
pkg_check_modules(XCB_COMP xcb-composite)
pkg_check_modules(XCB_UTIL xcb-util)
pkg_check_modules(XCB_DAMAGE xcb-damage)
pkg_check_modules(XCB_XTEST xcb-xtest)
pkg_check_modules(XCB_XKB xcb-xkb)
pkg_check_modules(XCB_RENDER xcb-render)
pkg_check_modules(XCB_RENDERUTIL xcb-renderutil)
pkg_check_modules(XCB_CURSOR xcb-cursor)
pkg_check_modules(XCB_SHM xcb-shm)
pkg_check_modules(XCB_XFIXES xcb-xfixes)
pkg_check_modules(XCB_SHAPE xcb-shape)
set(PX_WM_LINK_LIBRARIES px_shared pthread ${XCB_LIBRARIES} ${XCB_COMP_LIBRARIES} ${XCB_UTIL_LIBRARIES} ${XCB_DAMAGE_LIBRARIES} ${XCB_XTEST_LIBRARIES} ${XCB_XKB_LIBRARIES} ${XCB_RENDER_LIBRARIES} ${XCB_RENDERUTIL_LIBRARIES} ${XCB_CURSOR_LIBRARIES} ${XCB_SHM_LIBRARIES} ${XCB_XFIXES_LIBRARIES} ${XCB_SHAPE_LIBRARIES} glog px_dip)

#px_wm
add_executable(px_wm ${PX_WM_SOURCE_FILES})
add_dependencies(px_wm ${PX_WM_DEPENDENCIES})
target_compile_options(px_wm PRIVATE ${ADDITIONAL_COMPILE_OPTIONS})
target_link_libraries(px_wm ${PX_WM_LINK_LIBRARIES} ${ADDITIONAL_LINK_OPTIONS})

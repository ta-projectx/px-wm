#include "3rdparty/xcbxkb_compat.h"
#include "common/x11.h"
#include "shared/bitwiseenumclass.h"
#include "shared/memory.h"
#include "xkb/xkbdesc.h"
#include <glog/logging.h>
#include <xkb/x11frontend.h>

using namespace std;
using namespace pxwm::common;
using namespace pxwm::sharedds;
using namespace pxwm::xkb;
using namespace px::memory;
using namespace px::bitwiseenumclass;

class X11Frontend::Priv {
public:
    xcb_connection_t* fconn;
    xcb_connection_t* bconn;

    Priv() = default;
    Priv(const Priv&) = delete;
    Priv& operator=(const Priv&) = delete;
};

X11Frontend::X11Frontend(xcb_connection_t* fconn, xcb_connection_t* bconn) {
    p = make_unique<Priv>();
    p->fconn = fconn;
    p->bconn = bconn;
}

X11Frontend::~X11Frontend() {}

bool X11Frontend::get_latch_lock_state(shared::FELatchLockStateReply& ret) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto states_reply = make_unique_malloc(
        xcb_xkb_get_state_reply(p->fconn, xcb_xkb_get_state(p->fconn, XCB_XKB_ID_USE_CORE_KBD), &err.pop()));
    if (err.get()) {
        return false;
    }
    ret.locked_mods = states_reply->lockedMods;
    ret.latched_mods = states_reply->latchedMods;
    ret.locked_group = states_reply->lockedGroup;
    ret.latched_group = states_reply->latchedGroup;
    return true;
}

bool X11Frontend::get_controls(shared::FEControlsReply& ret) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto fe_ctrl = make_unique_malloc(
        xcb_xkb_get_controls_reply(p->fconn, xcb_xkb_get_controls(p->fconn, XCB_XKB_ID_USE_CORE_KBD), &err.pop()));
    if (err.get()) {
        return false;
    }
    ret.enabled_controls = fe_ctrl->enabledControls;
    ret.sk_ax_options = fe_ctrl->accessXOption;
    ret.rk_repeat_interval = fe_ctrl->repeatInterval;
    ret.rk_repeat_delay = fe_ctrl->repeatDelay;
    return true;
}

bool X11Frontend::forward_desc_to_backend(sharedds::XkbDescUpdateFlag flag) {
    // do not process empty requests
    if (!enumval(flag)) {
        return true;
    }
    UniquePointerWrapper_free<xcb_xkb_get_map_reply_t> fmap_rep;
    // all errors will be logged by these respective functions
    if (!desc::forward_xkb_map(p->fconn, p->bconn, &fmap_rep.pop())) {
        return false;
    }
    if (enumval(flag & sharedds::XkbDescUpdateFlag::COMPAT_MAPS)) {
        if (!desc::forward_xkb_compat_map(p->fconn, p->bconn)) {
            return false;
        }
    }
    if (enumval(flag & sharedds::XkbDescUpdateFlag::NAMES)) {
        if (!desc::forward_xkb_names(p->fconn, p->bconn, fmap_rep.get()->firstType)) {
            return false;
        }
    }

    return true;
}
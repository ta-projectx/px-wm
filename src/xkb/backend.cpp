#include "xkb/backend.h"
#include "common/customxcb/customxcb.h"
#include "common/x11.h"
#include "shared/bitwiseenumclass.h"
#include "shared/memory.h"
#include <assert.h>
#include <glog/logging.h>
#include <xcb/xcbext.h>

using namespace std;
using namespace pxwm::sharedds;
using namespace pxwm::common;
using namespace pxwm::common::customxcb;
using namespace pxwm::xkb;
using namespace px::memory;
using namespace px::bitwiseenumclass;

class Backend::Priv {
public:
    xcb_connection_t* conn;
    shared_ptr<X11Frontend> fe_xkb;

    bool update_desc(XkbDescUpdateFlag flag);
    bool update_states(XkbStateUpdateFlag flag, XkbStateChangedEventInfo* evt_info = nullptr);
    bool update_controls(XkbControlUpdateFlag flag);
    /**
     * Reset all backend's state and groups to zero.
     * @return true if successful, false otherwise.
     */
    bool reset_backend_states();
    /**
     * Disable all XKB's accessibility controls on the backend, so that it doesn't interfere with frontend's
     * accessibility settings.
     * @return true if successful, false otherwise.
     */
    bool reset_backend_controls();
};

Backend::Backend(xcb_connection_t* bconn) {
    p = make_unique<Priv>();
    p->conn = bconn;
}

Backend::~Backend() {}

bool Backend::init(std::shared_ptr<X11Frontend> fe) {
    if (p->fe_xkb.get()) {
        // may not happen!
        assert(false);
        LOG(FATAL) << "Attempt to re-init an XKB backend.";
        return false;
    }
    p->fe_xkb = fe;
    // WARNING! The ordering here is important!
    if (!(p->reset_backend_controls() && update_backend(XkbUpdatedInfo::all()))) {
        LOG(ERROR) << "Cannot init an XKB backend.";
        return false;
    }
    return true;
}

bool Backend::update_backend(sharedds::XkbUpdatedInfo updated, XkbStateChangedEventInfo* state_info) {
    // all errors will be logged by the respective subfunctions and this function's caller.
    // update to DESC also implies STATES update.
    if (enumval(updated.flags & XkbUpdateFlag::DESC)) {
        xcb_flush(p->conn);
        if (!p->reset_backend_states()) {
            return false;
        }
        if (!p->update_desc(updated.desc_flags)) {
            return false;
        }
        if (!p->update_states(XkbStateUpdateFlag::ALL)) {
            return false;
        }
    } else if (enumval(updated.flags & XkbUpdateFlag::STATES)) {
        if (!p->update_states(updated.state_flags, state_info)) {
            return false;
        }
    }
    if (enumval(updated.flags & XkbUpdateFlag::CONTROLS)) {
        if (!p->update_controls(updated.ctrl_flags)) {
            return false;
        }
    }
    return true;
}

bool Backend::Priv::update_desc(XkbDescUpdateFlag flag) {
    return fe_xkb->forward_desc_to_backend(flag);
}

bool Backend::Priv::update_states(XkbStateUpdateFlag flag, XkbStateChangedEventInfo* evt_info) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    uint8_t mod_affect_lock, mod_lock, mod_affect_latch, mod_latch, group_lock, group_latch;
    // determine if we should request current state
    if (evt_info != nullptr) {
        flag &= ((evt_info->changed & XCB_XKB_STATE_PART_MODIFIER_LOCK ? XkbStateUpdateFlag::LOCKED_MODS :
                                                                         XkbStateUpdateFlag::NONE) |
            (evt_info->changed & XCB_XKB_STATE_PART_MODIFIER_LATCH ? XkbStateUpdateFlag::LATCHED_MODS :
                                                                     XkbStateUpdateFlag::NONE) |
            (evt_info->changed & XCB_XKB_STATE_PART_GROUP_LOCK ? XkbStateUpdateFlag::LOCKED_GROUP :
                                                                 XkbStateUpdateFlag::NONE) |
            (evt_info->changed & XCB_XKB_STATE_PART_GROUP_LATCH ? XkbStateUpdateFlag::LATCHED_GROUP :
                                                                  XkbStateUpdateFlag::NONE));
        mod_lock = enumval(flag & XkbStateUpdateFlag::LOCKED_MODS) ? evt_info->locked_mods : 0;
        mod_latch = enumval(flag & XkbStateUpdateFlag::LATCHED_MODS) ? evt_info->latched_mods : 0;
        group_lock = enumval(flag & XkbStateUpdateFlag::LOCKED_GROUP) ? evt_info->locked_group : 0;
        group_latch = enumval(flag & XkbStateUpdateFlag::LATCHED_GROUP) ? evt_info->latched_group : 0;
    } else {
        shared::FELatchLockStateReply states_reply;
        if (!fe_xkb->get_latch_lock_state(states_reply)) {
            LOG(ERROR) << "Error retreiving frontend's state";
            return false;
        }
        mod_lock = enumval(flag & XkbStateUpdateFlag::LOCKED_MODS) ? states_reply.locked_mods : 0;
        mod_latch = enumval(flag & XkbStateUpdateFlag::LATCHED_MODS) ? states_reply.latched_mods : 0;
        group_lock = enumval(flag & XkbStateUpdateFlag::LOCKED_GROUP) ? states_reply.locked_group : 0;
        group_latch = enumval(flag & XkbStateUpdateFlag::LATCHED_GROUP) ? states_reply.latched_group : 0;
    }
    // do not process empty requests
    if (!enumval(flag)) {
        return true;
    }
    // affect all mods
    mod_affect_lock = enumval(flag & XkbStateUpdateFlag::LOCKED_MODS) ? 0x7f : 0;
    mod_affect_latch = enumval(flag & XkbStateUpdateFlag::LATCHED_MODS) ? 0x7f : 0;

    // do it (xcb)!
    err.pop() = xcb_request_check(
        conn, xcb_custom_xkb_latch_lock_state_checked(conn, XCB_XKB_ID_USE_CORE_KBD, mod_affect_lock, mod_lock,
                  (enumval(flag & XkbStateUpdateFlag::LOCKED_GROUP) ? 1 : 0), group_lock, mod_affect_latch, mod_latch,
                  (enumval(flag & XkbStateUpdateFlag::LATCHED_GROUP) ? 1 : 0), group_latch));
    if (err.get()) {
        LOG(ERROR) << "Error setting backend's latch/lock state";
        return false;
    }
    xcb_flush(conn);
    return true;
}

bool Backend::Priv::update_controls(XkbControlUpdateFlag flag) {
    // do not process empty requests
    if (!enumval(flag)) {
        return true;
    }
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    // for perKeyRepeat in 'xcb_xkb_set_controls_checked'
    uint8_t empty_buffer[32];
    memset(empty_buffer, 0, sizeof(empty_buffer));
    // ControlsEnabled, StickyKeys
    uint32_t change_controls = 0;
    uint32_t affect_enabled = 0, enabled = 0;
    // TwoKeys + LatchToLock
    uint16_t ax_options = 0;
    uint16_t repeat_delay = 0, repeat_interval = 0;

    //"parse" the flag
    if (enumval(flag & (XkbControlUpdateFlag::REPEAT_KEY | XkbControlUpdateFlag::STICKY_KEYS))) {
        change_controls |= XCB_XKB_CONTROL_CONTROLS_ENABLED;
    }
    affect_enabled |= enumval(flag & XkbControlUpdateFlag::REPEAT_KEY) ? XCB_XKB_BOOL_CTRL_REPEAT_KEYS : 0;
    affect_enabled |= enumval(flag & XkbControlUpdateFlag::STICKY_KEYS) ? XCB_XKB_BOOL_CTRL_STICKY_KEYS : 0;
    change_controls |= enumval(flag & XkbControlUpdateFlag::STICKY_KEYS_OPTIONS) ? XCB_XKB_BOOL_CTRL_STICKY_KEYS : 0;
    change_controls |=
        enumval(flag & XkbControlUpdateFlag::REPEAT_KEY_DELAY_INTERVAL) ? XCB_XKB_BOOL_CTRL_REPEAT_KEYS : 0;

    // get frontend's controls
    shared::FEControlsReply fe_ctrl;
    if (!fe_xkb->get_controls(fe_ctrl)) {
        LOG(ERROR) << "Cannot retreive frontend's control.";
        return false;
    }

    // setup parameters to forward
    if (change_controls & XCB_XKB_CONTROL_CONTROLS_ENABLED) {
        enabled = fe_ctrl.enabled_controls & affect_enabled;
    }
    if (change_controls & XCB_XKB_BOOL_CTRL_STICKY_KEYS) {
        ax_options = fe_ctrl.sk_ax_options & (XCB_XKB_AX_OPTION_LATCH_TO_LOCK | XCB_XKB_AX_OPTION_TWO_KEYS);
    }
    if (change_controls & XCB_XKB_BOOL_CTRL_REPEAT_KEYS) {
        repeat_delay = fe_ctrl.rk_repeat_delay;
        repeat_interval = fe_ctrl.rk_repeat_interval;
    }

    err.pop() = xcb_request_check(conn, xcb_xkb_set_controls_checked(conn, XCB_XKB_ID_USE_CORE_KBD, 0, 0, 0, 0, 0, 0, 0,
                                            0, 0, 0, ax_options, affect_enabled, enabled, change_controls, repeat_delay,
                                            repeat_interval, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, empty_buffer));
    if (err.get()) {
        LOG(ERROR) << "Cannot set backend's control.";
        return false;
    }
    xcb_flush(conn);
    return true;
}

bool Backend::Priv::reset_backend_states() {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    uint8_t mod_affect = 127, mod = 0;
    err.pop() = xcb_request_check(conn, xcb_custom_xkb_latch_lock_state_checked(conn, XCB_XKB_ID_USE_CORE_KBD,
                                            mod_affect, mod, 1, 0, mod_affect, mod, 0, 0));
    if (err.get()) {
        LOG(ERROR) << "Error resetting backend's latch/lock state";
        return false;
    }
    xcb_flush(conn);
    return true;
}

bool Backend::Priv::reset_backend_controls() {
    UniquePointerWrapper_free<xcb_generic_error_t> err;

    // for perKeyRepeat in 'xcb_xkb_set_controls_checked'
    uint8_t empty_buffer[32];
    memset(empty_buffer, 0, sizeof(empty_buffer));

    uint32_t changed_ctrls = XCB_XKB_CONTROL_CONTROLS_ENABLED | XCB_XKB_BOOL_CTRL_ACCESS_X_KEYS;
    uint32_t affect_enabled_ctrls, enabled_ctrls;
    uint16_t ax_options = 0;
    affect_enabled_ctrls = 31;
    enabled_ctrls = 0;

    err.pop() = xcb_request_check(conn, xcb_xkb_set_controls_checked(conn, XCB_XKB_ID_USE_CORE_KBD, 0, 0, 0, 0, 0, 0, 0,
                                            0, 0, 0, ax_options, affect_enabled_ctrls, enabled_ctrls, changed_ctrls, 0,
                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, empty_buffer));
    if (err.get()) {
        LOG(ERROR) << "Cannot reset backend's control.";
        return false;
    }
    xcb_flush(conn);
    return true;
}
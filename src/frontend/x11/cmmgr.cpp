#include "frontend/x11/cmmgr.h"
#include "common/x11.h"
#include "shared/memory.h"
#include "shared/util.h"
#include <cstring>
#include <functional>
#include <map>
#include <sharedds/clientmsgdata.h>
#include <xcb/xcb_icccm.h>

using namespace std;
using namespace pxwm;
using namespace pxwm::sharedds;
using namespace pxwm::common::x11;
using namespace pxwm::frontend::x11;
using namespace px::memory;
using namespace px::util;

class CMMgr::Priv {
public:
    WIDTrans* wid_trans;
    xcb_connection_t* fconn;
    xcb_connection_t* bconn;
    /**
     * The associated functions will return true if the client message is valid and should be forwarded, or false if the
     * property is detected as invalid or should not be forwarded.
     */
    typedef map<xcb_atom_t, function<bool(CMMgr::Priv*, ClientMsg&)>> hdlr_map_t;
    hdlr_map_t forward_atom_hdlrs;
    hdlr_map_t backward_atom_hdlrs;
    void fill_functions();
    // forward
    bool hdl_fw_wm_change_state(ClientMsg& cm);
    bool hdl_fw_net_active_window(ClientMsg& cm);
    bool hdl_fw_net_wm_state(ClientMsg& cm);
    // backward
    bool hdl_bw_wm_protocols(ClientMsg& cm);

    Priv() = default;
    Priv(const Priv&) = delete;
    Priv& operator=(const Priv&) = delete;
};

CMMgr::CMMgr(WIDTrans* wid_trans, xcb_connection_t* fconn, xcb_connection_t* bconn) {
    p = make_unique<Priv>();
    p->wid_trans = wid_trans;
    p->fconn = fconn;
    p->bconn = bconn;
    p->fill_functions();
}

CMMgr::~CMMgr() {}

bool CMMgr::forward_cm(ClientMsg& cm) {
    Priv::hdlr_map_t::iterator it;
    // for non-root targets
    if (p->wid_trans->get_fw_id(cm.window) && check_and_get_mapped_value(p->forward_atom_hdlrs, cm.msg_type, it)) {
        return it->second(p.get(), cm);
    }
    return false;
}

bool CMMgr::backward_cm(ClientMsg& cm) {
    Priv::hdlr_map_t::iterator it;
    // for non-root targets
    if (p->wid_trans->get_bw_id(cm.window) && check_and_get_mapped_value(p->backward_atom_hdlrs, cm.msg_type, it)) {
        return it->second(p.get(), cm);
    }
    return false;
}

ClientMsg CMMgr::convert_from_event(xcb_client_message_event_t* evt) {
    ClientMsg ret;
    memset(&ret, 0, sizeof(ClientMsg));
    ret.format = evt->format;
    ret.window = evt->window;
    ret.msg_type = evt->type;
    ret.data = evt->data;
    return ret;
}

void CMMgr::Priv::fill_functions() {
    // forward
    forward_atom_hdlrs[get_atom(bconn, false, "WM_CHANGE_STATE")] = &Priv::hdl_fw_wm_change_state;
    forward_atom_hdlrs[get_atom(bconn, false, "_NET_ACTIVE_WINDOW")] = &Priv::hdl_fw_net_active_window;
    forward_atom_hdlrs[get_atom(bconn, false, "_NET_WM_STATE")] = &Priv::hdl_fw_net_wm_state;
    // backward
    backward_atom_hdlrs[get_atom(fconn, false, "WM_PROTOCOLS")] = &Priv::hdl_bw_wm_protocols;
}

bool CMMgr::Priv::hdl_fw_wm_change_state(ClientMsg& cm) {
    if (cm.data.data32[0] == XCB_ICCCM_WM_STATE_ICONIC) {
        cm.send_info.destination = wid_trans->get_fe_root_id();
        cm.send_info.propagate = false;
        cm.send_info.event_mask = XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT;
        cm.window = wid_trans->get_fw_id(cm.window);
        // target window isn't managed by us
        if (!cm.window) {
            return false;
        }
        cm.msg_type = get_atom(fconn, false, "WM_CHANGE_STATE");
        // no need to change data
        return true;
    } else {
        return false;
    }
}

bool CMMgr::Priv::hdl_fw_net_active_window(ClientMsg& cm) {
    cm.msg_type = get_atom(fconn, false, "_NET_ACTIVE_WINDOW");
    cm.window = wid_trans->get_fw_id(cm.window);
    // target window isn't managed by us
    if (!cm.window) {
        return false;
    }
    cm.data.data32[1] = 0;
    cm.data.data32[2] = wid_trans->get_fw_id(cm.data.data32[2]);
    cm.send_info.destination = wid_trans->get_fe_root_id();
    cm.send_info.propagate = false;
    cm.send_info.event_mask = XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT;
    return true;
}

bool CMMgr::Priv::hdl_fw_net_wm_state(ClientMsg& cm) {
    cm.msg_type = get_atom(fconn, false, "_NET_WM_STATE");
    cm.window = wid_trans->get_fw_id(cm.window);
    // target window isn't managed by us
    if (!cm.window) {
        return false;
    }
    // 0, 1 and 2 is ATOM
    for (int i = 0; i < 2; ++i) {
        cm.data.data32[i] =
            cm.data.data32[i] ? get_atom(fconn, false, get_atom_name(bconn, cm.data.data32[i])) : XCB_ATOM_NONE;
    }
    cm.send_info.destination = wid_trans->get_fe_root_id();
    cm.send_info.propagate = false;
    cm.send_info.event_mask = XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT;
    return true;
}

bool CMMgr::Priv::hdl_bw_wm_protocols(ClientMsg& cm) {
    if (cm.data.data32[0] == get_atom(fconn, false, "WM_TAKE_FOCUS")) {
        cm.send_info.destination = cm.window = wid_trans->get_bw_id(cm.window);
        // both propagate and event_mask are zero
        cm.msg_type = get_atom(bconn, false, "WM_PROTOCOLS");
        cm.data.data32[0] = get_atom(bconn, false, "WM_TAKE_FOCUS");
        // we're too lazy to actually retrieve / cache server time! most software doesn't care anyway ;)
        cm.data.data32[1] = XCB_CURRENT_TIME;
        return true;
    } else {
        return false;
    }
}
#include "common/x11.h"
#include "shared/memory.h"
#include "shared/util.h"
#include "shared/xcomponent.h"
#include <cstring>
#include <glog/logging.h>
#include <libcuckoo/cuckoohash_map.hh>
#include <shared/i18n.h>
#include <shared_mutex>
#include <vector>
// extension query
#include <3rdparty/xcbxkb_compat.h>
#include <xcb/composite.h>
#include <xcb/damage.h>
#include <xcb/shape.h>
#include <xcb/xfixes.h>
#include <xcb/xtest.h>

using namespace pxwm::common::x11;
using namespace px;
using namespace px::memory;
using namespace px::util;
using namespace std;

namespace pxwm {
    namespace common {
        namespace x11 {
            /**
             * Static class for storing and retrieving X atoms.
             */
            class AtomManager {
            public:
                AtomManager() = delete;
                AtomManager(const AtomManager&) = delete;
                AtomManager& operator=(const AtomManager&) = delete;
                static xcb_atom_t get_atom(xcb_connection_t* xconn, bool only_if_exists, const string name);
                static string get_atom_name(xcb_connection_t* xconn, xcb_atom_t atom);
                static void clear_cache(xcb_connection_t* xconn);

            private:
                typedef cuckoohash_map<string, xcb_atom_t> atom_cache_t;
                typedef cuckoohash_map<xcb_atom_t, string> atom_name_cache_t;

                static cuckoohash_map<xcb_connection_t*, int> conn_map;
                static shared_timed_mutex vec_ac_mtx;
                static vector<shared_ptr<atom_cache_t>> vec_atom_cache;
                static vector<shared_ptr<atom_name_cache_t>> vec_atom_name_cache;

                static int get_conn_idx(xcb_connection_t* xconn);
            };
        }
    }
}

bool pxwm::common::x11::init(const string& display, xcb_connection_t*& out_conn, xcb_screen_t*& out_screen) {
    int screen_num;

    out_conn = xcb_connect(display.empty() ? nullptr : display.c_str(), &screen_num);
    if (xcb_connection_has_error(out_conn)) {
        disconnect_display(out_conn);
        return false;
    }

    // get screen @ screen_num
    auto svr_setup = xcb_get_setup(out_conn);
    xcb_screen_iterator_t screen_iter = xcb_setup_roots_iterator(svr_setup);
    for (int i = 0; i < screen_num; ++i) {
        xcb_screen_next(&screen_iter);
    }
    out_screen = screen_iter.data;

    return true;
}

bool pxwm::common::x11::is_screen_color_supported(VisualMap* visualmap) {
    // backend only checking code
    // criteria: DEFAULT root visual must be either 24 or 32 deep, 8 bit per channel, and both scanline_pad and
    // bits_per_pixel for the default depth must be 32 bit.
    // root visual
    auto& default_visual = visualmap->default_visual();
    if (!is_visual_supported(default_visual)) {
        return false;
    }
    // make sure format and scanline_pad is supported for default depth pixmap
    if (!visualmap->get_pixmap_depth_bpp_format(default_visual.depth).count(32) ||
        !visualmap->get_pixmap_scanline_pad(default_visual.depth).count(32)) {
        return false;
    }
    // monochrome pixmap
    if (!visualmap->get_pixmap_depth_bpp_format(1).count(1)) {
        return false;
    }
    // everything is OK!
    return true;
}

bool pxwm::common::x11::connect_and_check_display(const string& display,
    shared_ptr<VisualMap>& visualmap,
    bool is_backend,
    xcb_connection_t*& out_conn,
    xcb_screen_t*& out_screen) {
    string dpystr = (display.size() ? display.c_str() : xutil::XDisplayComponent(nullptr).to_string().c_str());
    if (!x11::init(display, out_conn, out_screen)) {
        fprintf(stderr, _("Cannot connect to X server '%1$s'."), dpystr.c_str());
        fputc('\n', stderr);
        return false;
    }
    visualmap = make_shared<VisualMap>(out_conn, out_screen);
    // only check for backend connections
    if (is_backend && !x11::is_screen_color_supported(visualmap.get())) {
        fprintf(stderr, _("Backend X server '%1$s' visual configuration is unsupported."), dpystr.c_str());
        fputc('\n', stderr);
        return false;
    }
    return true;
}

bool pxwm::common::x11::is_visual_supported(const VisualInfo& visual) {
    switch (visual.depth) {
        case 24:
        case 32:
            // pass!
            break;
        default:
            return false;
    }
    switch (visual.visual_class) {
        case XCB_VISUAL_CLASS_TRUE_COLOR:
        case XCB_VISUAL_CLASS_DIRECT_COLOR:
            // pass!
            break;
        default:
            return false;
    }
    if (visual.bits_per_rgb_value != 8) {
        return false;
    }
    return true;
}

void pxwm::common::x11::disconnect_display(xcb_connection_t* conn) {
    xcb_disconnect(conn);
}

// AtomManager definitions
cuckoohash_map<xcb_connection_t*, int> pxwm::common::x11::AtomManager::conn_map{};
vector<shared_ptr<pxwm::common::x11::AtomManager::atom_cache_t>> pxwm::common::x11::AtomManager::vec_atom_cache{};
vector<shared_ptr<pxwm::common::x11::AtomManager::atom_name_cache_t>>
    pxwm::common::x11::AtomManager::vec_atom_name_cache{};
shared_timed_mutex pxwm::common::x11::AtomManager::vec_ac_mtx;

xcb_atom_t pxwm::common::x11::AtomManager::get_atom(xcb_connection_t* xconn, bool only_if_exists, const string name) {
    if (name.empty()) {
        return XCB_ATOM_NONE;
    }
    int connidx = get_conn_idx(xconn);

    // if we access the vector while it is reallocating, undefined behavior will occur
    shared_lock<shared_timed_mutex> vec_ac_lock(vec_ac_mtx);
    auto& atom_cache = vec_atom_cache[connidx];
    auto& atom_name_cache = vec_atom_name_cache[connidx];
    vec_ac_lock.unlock();

    if (atom_cache->contains(name)) {
        return atom_cache->find(name);
    }

    // not in cache
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto cookie = xcb_intern_atom(xconn, (only_if_exists ? 1 : 0), name.size(), name.c_str());
    auto reply = make_unique_malloc<xcb_intern_atom_reply_t>(xcb_intern_atom_reply(xconn, cookie, &err.pop()));
    if (err.get() == nullptr && reply.get() && reply->atom != XCB_ATOM_NONE) {
        // bidirectional store
        atom_cache->insert(name, reply->atom);
        atom_name_cache->insert(reply->atom, name);
        // OK!
        return reply->atom;
    } else {
        return XCB_ATOM_NONE;
    }
}

string pxwm::common::x11::AtomManager::get_atom_name(xcb_connection_t* xconn, xcb_atom_t atom) {
    int connidx = get_conn_idx(xconn);

    if (!atom) {
        // NONE
        return "";
    }

    // if we access the vector while it is reallocating, undefined behavior will occur
    shared_lock<shared_timed_mutex> vec_ac_lock(vec_ac_mtx);
    auto& atom_name_cache = vec_atom_name_cache[connidx];
    auto& atom_cache = vec_atom_cache[connidx];
    vec_ac_lock.unlock();

    if (atom_name_cache->contains(atom)) {
        return atom_name_cache->find(atom);
    } else {
        UniquePointerWrapper_free<xcb_generic_error_t> err;
        auto atom_name_r =
            make_unique_malloc(xcb_get_atom_name_reply(xconn, xcb_get_atom_name(xconn, atom), &err.pop()));
        if (err.get() == nullptr && atom_name_r.get() != nullptr) {
            string atom_name(
                xcb_get_atom_name_name(atom_name_r.get()), xcb_get_atom_name_name_length(atom_name_r.get()));
            atom_name_cache->insert(atom, atom_name);
            atom_cache->insert(atom_name, atom);
            return atom_name;
        }
    }

    // error / no match
    return "";
}

void pxwm::common::x11::AtomManager::clear_cache(xcb_connection_t* xconn) {
    if (conn_map.contains(xconn)) {
        int connidx = get_conn_idx(xconn);
        unique_lock<shared_timed_mutex> lock(vec_ac_mtx);
        // clean the data
        vec_atom_cache[connidx] = nullptr;
        vec_atom_name_cache[connidx] = nullptr;
        // dereference
        conn_map.erase(xconn);
    }
}

int pxwm::common::x11::AtomManager::get_conn_idx(xcb_connection_t* xconn) {
    if (!conn_map.contains(xconn)) {
        unique_lock<shared_timed_mutex> lock(vec_ac_mtx);
        // make sure we're not going to recreate this xconn ID.
        if (conn_map.contains(xconn)) {
            return conn_map.find(xconn);
        }
        // OK, no one indeed doesn't try to create an ID for this xconn.
        int id;
        id = vec_atom_cache.size();
        // bidirectional cache
        vec_atom_cache.push_back(make_shared<atom_cache_t>());
        vec_atom_name_cache.push_back(make_shared<atom_name_cache_t>());
        conn_map.insert(xconn, id);
        return id;
    } else {
        return conn_map.find(xconn);
    }
}

xcb_atom_t pxwm::common::x11::get_atom(xcb_connection_t* xconn, bool only_if_exists, const string name) {
    return AtomManager::get_atom(xconn, only_if_exists, name);
}

string pxwm::common::x11::get_atom_name(xcb_connection_t* xconn, const xcb_atom_t atom) {
    return AtomManager::get_atom_name(xconn, atom);
}

xcb_atom_t pxwm::common::x11::translate_atom(xcb_connection_t* src_conn, xcb_connection_t* dst_conn, xcb_atom_t atom) {
    if (atom) {
        return get_atom(dst_conn, false, get_atom_name(src_conn, atom));
    } else {
        return 0;
    }
}

void pxwm::common::x11::translate_atoms(xcb_connection_t* src_conn,
    xcb_connection_t* dst_conn,
    uint32_t atoms_count,
    xcb_atom_t* atoms) {
    for (int i = 0; i < atoms_count; ++i) {
        if (atoms[i]) {
            atoms[i] = translate_atom(src_conn, dst_conn, atoms[i]);
        }
    }
}

void pxwm::common::x11::clear_atom_cache(xcb_connection_t* xconn) {
    AtomManager::clear_cache(xconn);
}

bool pxwm::common::x11::query_extensions(xcb_connection_t* xconn,
    std::set<xcb_extension_t*> ext_ids,
    std::map<xcb_extension_t*, XExtensionInfo>* ret) {
    for (auto ext_id : ext_ids) {
        XExtensionInfo info;
        const xcb_query_extension_reply_t* rep = xcb_get_extension_data(xconn, ext_id);
        if (!rep || !rep->present) {
            LOG(WARNING) << "One or more required X extensions not found.";
            return false;
        }
        if (ret) {
            info.first_event = rep->first_event;
            info.first_error = rep->first_error;
            info.opcode = rep->major_opcode;
            (*ret)[ext_id] = info;
        }
    }
    return true;
}

bool pxwm::common::x11::init_and_check_extensions_version(xcb_connection_t* xconn, std::set<xcb_extension_t*> ext_ids) {
    int count = 0;
    UniquePointerWrapper_free<xcb_generic_error_t> err;

    auto print_err_msg = [](const char* ext_name, pair<int, int> req_ver) {
        printf(_("Requires %1$s extension version %2$d.%3$d or newer on the display server."), ext_name, req_ver.first,
            req_ver.second);
        puts("");
    };
    pair<int, int> req_ver;

    // no need for error checking here. caller should request extensions that are guaranteed to exist.

    // composite
    if (ext_ids.count(&xcb_composite_id)) {
        ++count;
        req_ver = {0, 4};
        UniquePointerWrapper_free<xcb_generic_error_t> err;
        auto composite_version = make_unique_malloc(xcb_composite_query_version_reply(
            xconn, xcb_composite_query_version(xconn, req_ver.first, req_ver.second), &err.pop()));
        if (err.get() == nullptr && composite_version->major_version <= req_ver.first &&
            composite_version->minor_version < req_ver.second) {
            print_err_msg("Composite", req_ver);
            return false;
        }
    }

    // damage
    if (ext_ids.count(&xcb_damage_id)) {
        ++count;
        req_ver = {1, 1};
        auto damage_version = make_unique_malloc(xcb_damage_query_version_reply(
            xconn, xcb_damage_query_version(xconn, req_ver.first, req_ver.second), &err.pop()));
        if (err.get() == nullptr && damage_version->major_version <= req_ver.first &&
            damage_version->minor_version < req_ver.second) {
            print_err_msg("DAMAGE", req_ver);
            return false;
        }
    }

    // xtest
    if (ext_ids.count(&xcb_test_id)) {
        ++count;
        req_ver = {2, 0};
        auto xtest_version = make_unique_malloc(
            xcb_test_get_version_reply(xconn, xcb_test_get_version(xconn, req_ver.first, req_ver.second), &err.pop()));
        if (err.get() == nullptr && xtest_version->major_version < req_ver.first &&
            xtest_version->minor_version < req_ver.second) {
            print_err_msg("XTEST", req_ver);
            return false;
        }
    }

    // xkb
    if (ext_ids.count(&xcb_xkb_id)) {
        ++count;
        req_ver = {1, 0};
        auto xkb_version = make_unique_malloc(xcb_xkb_use_extension_reply(
            xconn, xcb_xkb_use_extension(xconn, req_ver.first, req_ver.second), &err.pop()));
        if (err.get() == nullptr && xkb_version->serverMajor < req_ver.first &&
            xkb_version->serverMinor < req_ver.second) {
            print_err_msg("XKB", req_ver);
            return false;
        }
    }

    // xrender
    if (ext_ids.count(&xcb_render_id)) {
        ++count;
        req_ver = {0, 11};
        auto xrender_version = make_unique_malloc(xcb_render_query_version_reply(
            xconn, xcb_render_query_version(xconn, req_ver.first, req_ver.second), &err.pop()));
        if (err.get() == nullptr && xrender_version->major_version < req_ver.first &&
            xrender_version->minor_version < req_ver.second) {
            print_err_msg("RENDER", req_ver);
            return false;
        }
    }

    // xfixes
    if (ext_ids.count(&xcb_xfixes_id)) {
        ++count;
        req_ver = {2, 0};
        auto xfixes_version = make_unique_malloc(xcb_xfixes_query_version_reply(
            xconn, xcb_xfixes_query_version(xconn, req_ver.first, req_ver.second), &err.pop()));
        if (err.get() == nullptr && xfixes_version->major_version < req_ver.first) {
            print_err_msg("XFIXES", req_ver);
            return false;
        }
    }

    // xshape
    if (ext_ids.count(&xcb_shape_id)) {
        ++count;
        req_ver = {1, 1};
        auto shape_version =
            make_unique_malloc(xcb_shape_query_version_reply(xconn, xcb_shape_query_version(xconn), &err.pop()));
        if (err.get() == nullptr && shape_version->major_version < req_ver.first &&
            shape_version->minor_version < req_ver.second) {
            print_err_msg("SHAPE", req_ver);
            return false;
        }
    }

    // this error will be logged if caller request version checking for undefined extensions.
    if (count != ext_ids.size()) {
        LOG(WARNING) << (ext_ids.size() - count)
                     << " requested extension versions isn't supported and can't be queried.";
        return false;
    }
    return true;
}

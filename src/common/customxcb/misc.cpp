#include "common/customxcb/customxcb.h"

using namespace pxwm::common;

xcb_generic_event_t* customxcb::xcb_timed_wait_for_event(xcb_connection_t* c, timespec timeout) {
    // zero timeout is basically the same as poll_for_event!
    if (timeout.tv_nsec == 0 && timeout.tv_sec == 0) {
        return xcb_poll_for_event(c);
    }

    // check if there is an event already in the event queue. immediately return if it is the case.
    if (auto evt = xcb_poll_for_event(c)) {
        return evt;
    }

    // otherwise, we will wait for the XCB's FD for incoming data
    int xcb_fd = xcb_get_file_descriptor(c);
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(xcb_fd, &fds);
    // wait for specified time or until a data arrives on the FD
    pselect(xcb_fd + 1, &fds, nullptr, nullptr, &timeout, nullptr);
    // if null, then pselect was timed out
    return xcb_poll_for_event(c);
}
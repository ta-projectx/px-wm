#include "common/eventpooler.h"
#include "common/customxcb/customxcb.h"
#include "shared/memory.h"
#include "shared/util.h"
#include <algorithm>
#include <cstring>
#include <glog/logging.h>
#include <queue>
#include <vector>
#include <xcb/xcb.h>

using namespace std;
using namespace px::memory;
using namespace px::util;
using namespace pxwm::common::x11;

class EventPooler::Priv {
public:
    xcb_connection_t* conn;
    /**
     * Maximum size of function lookup table. Corresponds to the highest number of event response type that will be
     * handled.
     */
    uint8_t size;
    uint8_t count;
    vector<bool> has_handler;
    vector<function<void(xcb_generic_event_t*, int, bool)>> eh_tbl;
    /**
     * Count how many event(s) existed for a particular event type in the current queue.
     */
    vector<int> evt_counter;
    /**
     * Contains default value for evt_counter.
     */
    vector<int> evt_default_counter;
    /**
     * Elapsed count for a particular event type.
     */
    vector<int> evt_elapsed;
    /**
     * Stores all events in current queue.
     */
    vector<unique_ptr<xcb_generic_event_t, free_delete>> evt_store;
    /**
     * Storeas all unhandled events by a call to custom_event_poll.
     */
    queue<xcb_generic_event_t*> custom_queued_evts;

    xcb_generic_event_t* poll_event_combo();

    Priv() = default;
    Priv(const Priv&) = delete;
    Priv& operator=(const Priv&) = delete;
};

EventPooler::EventPooler(xcb_connection_t* conn,
    std::map<uint8_t, std::function<void(xcb_generic_event_t*, int, bool)>>& event_handlers) {
    p = make_unique<Priv>();
    p->conn = conn;
    // build an instant lookup table (uint8_t is guaranteed not to exceed 255)
    for (auto kvp : event_handlers) {
        p->count = max(p->count, kvp.first);
    }
    ++p->count;
    p->size = 128;
    p->eh_tbl.resize(p->size);
    p->has_handler.resize(p->size, false);
    p->evt_counter.resize(p->size, -1);
    p->evt_default_counter.resize(p->size, -1);
    p->evt_elapsed.resize(p->size);
    for (auto kvp : event_handlers) {
        p->eh_tbl[kvp.first] = kvp.second;
        p->has_handler[kvp.first] = true;
    }
}

EventPooler::~EventPooler() {}

bool EventPooler::poll_for_events() {
    // for paranoid micro ops-er! :-)
    function<xcb_generic_event_t*()> event_poller_fn;
    if (p->custom_queued_evts.size()) {
        event_poller_fn = bind(&Priv::poll_event_combo, p.get());
    } else {
        event_poller_fn = bind(xcb_poll_for_event, p->conn);
    }

    unique_ptr<xcb_generic_event_t, free_delete> evt;
    evt.reset(event_poller_fn());
    // check for error
    if (evt.get() == nullptr) {
        int xcb_err = xcb_connection_has_error(p->conn);
        if (xcb_err) {
            LOG(ERROR) << "XCB error code: " << xcb_err;
            return false;
        }
    } else {
        p->evt_store.clear();
        memcpy(&p->evt_counter[0], &p->evt_default_counter[0], sizeof(int) * p->count);
        memset(&p->evt_elapsed[0], 0, sizeof(int) * p->count);

        // retrieve all events in this queue and count them.
        do {
            ++p->evt_counter[evt->response_type & 0x7f];
            p->evt_store.push_back(move(evt));
            evt.reset(event_poller_fn());
        } while (evt.get() != nullptr);

        // execute event handlers one by one
        for (auto& ievt : p->evt_store) {
            const int evt_type = ievt->response_type & 0x7f;
            if (p->has_handler[evt_type]) {
                // warning! take care of expr evaluation order
                p->eh_tbl[evt_type](
                    ievt.get(), p->evt_elapsed[evt_type], p->evt_elapsed[evt_type] == p->evt_counter[evt_type]);
                ++p->evt_elapsed[evt_type];
            }
        }
    }
    return true;
}

void EventPooler::custom_wait_for_event(std::function<bool(const xcb_generic_event_t*)> handler, timespec timeout) {
    auto evt = customxcb::xcb_timed_wait_for_event(p->conn, timeout);
    bool status = false;

    DeleteInvoker cleaner([&] {
        // must not free/handle null events
        if (evt) {
            if (!status) {
                // handler doesn't handle this event. let the main handler handles this event.
                p->custom_queued_evts.push(evt);
            } else {
                free(evt);
            }
        }
    });

    status = handler(evt);
}

xcb_generic_event_t* EventPooler::Priv::poll_event_combo() {
    xcb_generic_event_t* ret;
    if (custom_queued_evts.size()) {
        ret = custom_queued_evts.front();
        custom_queued_evts.pop();
    } else {
        ret = xcb_poll_for_event(conn);
    }
    return ret;
}
#include "common/xraii/xrenderpicture.h"
#include "shared/memory.h"
#include <glog/logging.h>
#include <xcb/xcb_util.h>

using namespace px::memory;
using namespace pxwm::common::xraii;

XRenderPicture::XRenderPicture() : conn{nullptr}, id{0} {}

XRenderPicture::XRenderPicture(xcb_connection_t* conn,
    xcb_drawable_t drawable,
    xcb_render_pictformat_t format,
    uint32_t value_mask,
    const uint32_t* value_list)
    : conn{conn}, id{init(conn, drawable, format, value_mask, value_list)} {}

XRenderPicture::~XRenderPicture() {
    if (conn && id) {
        xcb_render_free_picture(conn, id);
    }
}

xcb_render_picture_t XRenderPicture::init(xcb_connection_t* conn,
    xcb_drawable_t drawable,
    xcb_render_pictformat_t format,
    uint32_t value_mask,
    const uint32_t* value_list) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    xcb_render_picture_t ret = xcb_generate_id(conn);
    err.pop() =
        xcb_request_check(conn, xcb_render_create_picture_checked(conn, ret, drawable, format, value_mask, value_list));
    if (err.get()) {
        LOG(ERROR) << "Cannot create X Render Picture: " << xcb_event_get_error_label(err.get()->error_code);
        return 0;
    }
    return ret;
}
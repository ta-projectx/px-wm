#include "common/xraii/colormap.h"
#include "shared/memory.h"
#include <glog/logging.h>
#include <xcb/xcb_util.h>

using namespace px::memory;
using namespace pxwm::common::xraii;

ColorMap::ColorMap() : conn{nullptr}, id{0} {}

ColorMap::ColorMap(xcb_connection_t* conn, xcb_window_t window, xcb_visualid_t visual)
    : conn{conn}, id{init(conn, window, visual)} {}

ColorMap::~ColorMap() {
    if (conn && id) {
        xcb_free_colormap(conn, id);
    }
}

xcb_colormap_t ColorMap::init(xcb_connection_t* conn, xcb_window_t window, xcb_visualid_t visual) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    xcb_colormap_t ret = xcb_generate_id(conn);
    err.pop() =
        xcb_request_check(conn, xcb_create_colormap_checked(conn, XCB_COLORMAP_ALLOC_NONE, ret, window, visual));
    if (err.get()) {
        LOG(ERROR) << "Cannot create ColorMap: " << xcb_event_get_error_label(err.get()->error_code);
        return 0;
    }
    return ret;
}
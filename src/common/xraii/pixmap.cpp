#include "common/xraii/pixmap.h"
#include "shared/memory.h"
#include <glog/logging.h>

using namespace px::memory;
using namespace pxwm::common::xraii;

Pixmap::Pixmap() : conn{nullptr}, id{0} {}

Pixmap::Pixmap(xcb_connection_t* conn, xcb_pixmap_t pxm_id) : conn{conn}, id{pxm_id} {}

Pixmap::Pixmap(xcb_connection_t* conn, uint8_t depth, xcb_drawable_t drawable, uint16_t width, uint32_t height)
    : conn{conn}, id{init(conn, depth, drawable, width, height)} {}

Pixmap::~Pixmap() {
    if (conn && id) {
        xcb_free_pixmap(conn, id);
    }
}

xcb_pixmap_t Pixmap::init(xcb_connection_t* conn,
    uint8_t depth,
    xcb_drawable_t drawable,
    uint16_t width,
    uint32_t height) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    xcb_pixmap_t ret = xcb_generate_id(conn);
    err.pop() = xcb_request_check(conn, xcb_create_pixmap(conn, depth, ret, drawable, width, height));
    if (err.get()) {
        LOG(ERROR) << "Cannot create pixmap";
        return 0;
    }
    return ret;
}
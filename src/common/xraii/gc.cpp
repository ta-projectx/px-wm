#include "common/xraii/gc.h"
#include "shared/memory.h"
#include <glog/logging.h>
#include <xcb/xcb_util.h>

using namespace px::memory;
using namespace pxwm::common::xraii;

GC::GC() : conn{nullptr}, id{0} {}

GC::GC(xcb_connection_t* conn, xcb_drawable_t drawable, uint32_t value_mask, const uint32_t* value_list)
    : conn{conn}, id{init(conn, drawable, value_mask, value_list)} {}

GC::~GC() {
    if (conn && id) {
        xcb_free_gc(conn, id);
    }
}

xcb_gcontext_t GC::init(xcb_connection_t* conn,
    xcb_drawable_t drawable,
    uint32_t value_mask,
    const uint32_t* value_list) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    xcb_gcontext_t ret = xcb_generate_id(conn);
    err.pop() = xcb_request_check(conn, xcb_create_gc_checked(conn, ret, drawable, value_mask, value_list));
    if (err.get()) {
        LOG(ERROR) << "Cannot create GC: " << xcb_event_get_error_label(err.get()->error_code);
        return 0;
    }
    return ret;
}
#include "common/xraii/shm.h"
#include "shared/memory.h"
#include <sys/shm.h>
#include <xcb/shm.h>
#include <xcb/xcb_image.h>

using namespace px::memory;
using namespace pxwm;
using namespace pxwm::common::xraii;

SHM::SHM(xcb_connection_t* conn, uint32_t size) : conn{conn}, size{size}, info{generate_new()}, success{init()} {}

SHM::SHM(xcb_connection_t* conn, uint32_t size, int shmid)
    : conn{conn}, size{size}, info{generate_existing(shmid)}, success{init()} {}

SHM::~SHM() {
    // make sure we've correctly init'ed before destroying anything
    if (success) {
        xcb_shm_detach(conn, info.shmseg);
        shmdt(info.shmaddr);
    }
}

xcb_shm_segment_info_t SHM::generate_new() {
    xcb_shm_segment_info_t ret{0, 0, 0};
    int shmid = shmget(IPC_PRIVATE, size, IPC_CREAT | 0777);
    if (shmid >= 0) {
        valid_shmid = true;
        ret.shmid = shmid;
        ret.shmaddr = (uint8_t*)shmat(ret.shmid, 0, 0);
        ret.shmseg = xcb_generate_id(conn);
        // destroy SHM segment after last process detach it
        shmctl(ret.shmid, IPC_RMID, 0);
    } else {
        valid_shmid = false;
    }
    return ret;
}

xcb_shm_segment_info_t SHM::generate_existing(int shmid) {
    xcb_shm_segment_info_t ret;
    valid_shmid = true;
    ret.shmid = shmid;
    ret.shmaddr = (uint8_t*)shmat(ret.shmid, 0, 0);
    ret.shmseg = xcb_generate_id(conn);
    // no setting IPC_RMID, because the "owner" may or may not have set it.
    return ret;
}

bool SHM::init() {
    // does SHM generation succeed?
    if (!valid_shmid) {
        return false;
    }
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    err.pop() = xcb_request_check(conn, xcb_shm_attach_checked(conn, info.shmseg, info.shmid, 0));
    if (err.get()) {
        // server error condition. detach SHM seg created earlier
        shmdt(info.shmaddr);
        return false;
    }
    return true;
}
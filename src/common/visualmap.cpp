#include "common/visualmap.h"
#include "common/xraii/colormap.h"
#include "shared/memory.h"
#include "shared/util.h"
#include <unordered_map>
#include <xcb/xcb_util.h>

using namespace std;
using namespace px::util;
using namespace px::memory;
using namespace pxwm::common::x11;
using namespace pxwm::common;

class VisualMap::Priv {
public:
    typedef unordered_map<xcb_visualid_t, VisualInfo> visual_map_t;
    typedef unordered_map<uint8_t, unordered_set<uint8_t>> pixmap_map_t;
    xcb_connection_t* conn;
    xcb_screen_t* scr;
    vector<VisualInfo> visuals;
    visual_map_t visual_map;
    pixmap_map_t pixmap_depth_map;
    pixmap_map_t pixmap_scanline_pad;
    VisualInfo default_visual;
    unordered_map<uint8_t, VisualInfo> match_visual_depth_cache;
    unordered_map<xcb_visualid_t, shared_ptr<xraii::ColorMap>> visual_colormap;

    unordered_set<uint8_t>& get_pixmap_map_data(pixmap_map_t& map, uint8_t depth);
};

VisualMap::VisualMap(xcb_connection_t* conn, xcb_screen_t* scr) {
    p = make_unique<Priv>();
    p->conn = conn;
    p->scr = scr;
    // visual map
    auto depth_iter = xcb_screen_allowed_depths_iterator(scr);
    while (depth_iter.rem) {
        auto visual_iter = xcb_depth_visuals_iterator(depth_iter.data);
        while (visual_iter.rem) {
            auto vdata = visual_iter.data;
            VisualInfo vinfo{vdata->visual_id, vdata->_class, depth_iter.data->depth, vdata->bits_per_rgb_value};
            p->visual_map[vdata->visual_id] = vinfo;
            p->visuals.push_back(vinfo);
            xcb_visualtype_next(&visual_iter);
        }
        xcb_depth_next(&depth_iter);
    }
    // pixmap depth/formats
    auto pxm_fmt_it = xcb_setup_pixmap_formats_iterator(xcb_get_setup(conn));
    while (pxm_fmt_it.rem) {
        p->pixmap_depth_map[pxm_fmt_it.data->depth].emplace(pxm_fmt_it.data->bits_per_pixel);
        p->pixmap_scanline_pad[pxm_fmt_it.data->depth].emplace(pxm_fmt_it.data->scanline_pad);
        xcb_format_next(&pxm_fmt_it);
    }
    // default
    p->default_visual = p->visual_map[scr->root_visual];
}

VisualMap::~VisualMap() {}

const std::vector<VisualInfo>& VisualMap::available_visuals() {
    return p->visuals;
}

const VisualInfo VisualMap::get_visual_type(xcb_visualid_t id) {
    static VisualInfo empty{0, 0, 0, 0};
    Priv::visual_map_t::iterator it;
    if (check_and_get_mapped_value(p->visual_map, id, it)) {
        return it->second;
    } else {
        return empty;
    }
}

const VisualInfo VisualMap::get_visual_type_by_depth(uint8_t depth) {
    unordered_map<uint8_t, VisualInfo>::iterator it;
    if (check_and_get_mapped_value(p->match_visual_depth_cache, depth, it)) {
        return it->second;
    }
    // look for 8 bit per channel, TrueColor (prioritized) or DirectColor
    xcb_visualid_t found = 0, found_prio = 0;
    // favor root's default
    if (p->default_visual.depth == depth && p->default_visual.bits_per_rgb_value == 8 &&
        (p->default_visual.visual_class == XCB_VISUAL_CLASS_DIRECT_COLOR ||
            p->default_visual.visual_class == XCB_VISUAL_CLASS_TRUE_COLOR)) {
        found_prio = p->default_visual.id;
    } else {
        for (auto kvp : p->visual_map) {
            if (kvp.second.depth == depth && kvp.second.bits_per_rgb_value == 8) {
                if (kvp.second.visual_class == XCB_VISUAL_CLASS_TRUE_COLOR) {
                    found_prio = kvp.first;
                    break;
                } else if (!found && kvp.second.visual_class == XCB_VISUAL_CLASS_DIRECT_COLOR) {
                    found = kvp.first;
                }
            }
        }
    }
    VisualInfo ret;
    if (found_prio) {
        ret = get_visual_type(found_prio);
    } else if (found) {
        ret = get_visual_type(found);
    } else {
        ret = {0, 0, 0, 0};
    }
    // cache
    p->match_visual_depth_cache[depth] = ret;
    return ret;
}

const VisualInfo VisualMap::default_visual() {
    return p->default_visual;
}

xcb_colormap_t VisualMap::get_default_visual_colormap(xcb_visualid_t visualid) {
    if (!p->visual_map.count(visualid)) {
        return 0;
    }
    unordered_map<xcb_visualid_t, shared_ptr<xraii::ColorMap>>::iterator cit;
    if (check_and_get_mapped_value(p->visual_colormap, visualid, cit)) {
        return cit->second->id;
    }
    // create a new one
    p->visual_colormap[visualid] = make_shared<xraii::ColorMap>(p->conn, p->scr->root, visualid);
    return p->visual_colormap[visualid]->id;
}

const unordered_set<uint8_t>& VisualMap::get_pixmap_depth_bpp_format(uint8_t depth) {
    return p->get_pixmap_map_data(p->pixmap_depth_map, depth);
}

const std::unordered_set<uint8_t>& VisualMap::get_pixmap_scanline_pad(uint8_t depth) {
    return p->get_pixmap_map_data(p->pixmap_scanline_pad, depth);
}

unordered_set<uint8_t>& VisualMap::Priv::get_pixmap_map_data(pixmap_map_t& map, uint8_t depth) {
    static unordered_set<uint8_t> empty{};
    Priv::pixmap_map_t::iterator it;
    if (check_and_get_mapped_value(map, depth, it)) {
        return it->second;
    } else {
        return empty;
    }
}
#include "backend/backend.h"
#include "backend/common.h"
#include "backend/eventloop.h"
#include "common/stringconsts.h"
#include "common/visualmap.h"
#include "common/x11.h"
#include "shared/i18n.h"
#include "shared/util.h"
#include "shared/xcomponent.h"
#include "sighandlers.h"
#include <shared/memory.h>
#include <sys/syscall.h>

#include <cstring>
#include <map>
#include <signal.h>
#include <string>
#include <thread>
#include <utility>

#include "3rdparty/xcbxkb_compat.h"
#include <xcb/composite.h>
#include <xcb/damage.h>
#include <xcb/render.h>
#include <xcb/shape.h>
#include <xcb/xcb.h>
#include <xcb/xcb_cursor.h>
#include <xcb/xcb_renderutil.h>
#include <xcb/xcb_util.h>
#include <xcb/xtest.h>

#include "3rdparty/cameron314/concurrentqueue.h"
#include <glog/logging.h>
#include <libcuckoo/cuckoohash_map.hh>

using namespace std;
using namespace px;
using namespace px::memory;
using namespace px::util;
using namespace pxwm;
using namespace pxwm::backend;
using namespace pxwm::backend::eventloop;
using namespace pxwm::states;
using namespace pxwm::common;

class Backend::Priv {
private:
    Backend* const parent;

public:
    Priv(Backend* p, shared_ptr<Config> pcfg) : parent{p}, cfg{pcfg} {}
    Priv(Priv&) = delete;
    Priv& operator=(const Priv&) = delete;

    typedef map<xutil::XDisplayComponent, shared_ptr<EventLoop>> eventloop_instance_map_t;

    xutil::XDisplayComponent fe_dpy_component = xutil::XDisplayComponent(nullptr);
    eventloop_instance_map_t eventloop_instances;
    shared_ptr<condition_variable> cv_be_loop;
    shared_ptr<Config> cfg;
    enum class ExtCommands { NONE, INFO, ADD_BK, REMOVE_BK, GET_BK_LIST, GET_BK_INFO, GET_MANAGED_WINDOWS, RESCALE_BK };
    struct {
        atomic<ExtCommands> cmd;
        vector<string> cmd_params;
        // this also acts as a synchronisation barrier!
        atomic<bool> ret_finished;
        // return values
        atomic<bool> ret_status;
        vector<string> ret_bk_displays;
        vector<pair<xcb_window_t, xcb_window_t>> ret_managed_windows_list;
        pxwm::states::BkFrontEndOptions ret_bk_info;

    } ext_caller;

    bool add_and_connect_bk_display(const string& display, BkFrontEndOptions options);
    bool connect_bk_display(const string& display,
        xcb_connection_t*& conn,
        xcb_screen_t*& screen,
        shared_ptr<x11::VisualMap>& visualmap);
    /**
     * @brief disconnect a backend display and remove it from this backend's management.
     * @param display_string Display no. to disconnect and remove from this backend.
     * @return true if success, false if the given display isn't managed by this backend.
     */
    bool delete_bk_display(string display_string);
    bool init_composite_redir(xcb_connection_t* conn, xcb_screen_t* screen);
    bool select_backend_xkb_events(xcb_connection_t* conn);
    void set_backend_default_cursor(xcb_connection_t* conn, xcb_screen_t* screen);
    bool select_backend_xfixes_cursor(xcb_connection_t* conn, xcb_screen_t* screen);
    vector<xcb_window_t> query_existing_backend_windows(xcb_connection_t* conn,
        xcb_screen_t* screen,
        x11::VisualMap* visualmap);
    void reset_backend(xcb_connection_t* conn, xcb_screen_t* scr);
    void call_extfns(ExtCommands cmd, initializer_list<string> params);
    void handle_ext_add_bk_display();
    void handle_ext_remove_bk_display();
    void handle_ext_get_bk_displays();
    void handle_ext_get_bk_info();
    void handle_ext_get_managed_windows();
    void handle_ext_rescale_bk();
    void print_server_stop_cause(intermsg::eventloopbackend::StopMessage msg, string display);

    struct FnWindowMapped;
    struct FnWindowUnmapped;
};

Backend::Backend(const shared_ptr<Config> cfg) {
    p = make_unique<Priv>(this, cfg);
    p->ext_caller.cmd.store(Priv::ExtCommands::NONE, memory_order_relaxed);
    p->cv_be_loop = make_shared<condition_variable>();
}

Backend::~Backend() {}

void Backend::run() {
    // is frontend display specified?
    if (p->fe_dpy_component.is_invalid()) {
        LOG(ERROR) << "Invalid frontend display specification.";
        puts(_("Invalid frontend display specification."));
        return;
    } else {
        LOG(INFO) << "Frontend display is '" << p->fe_dpy_component.to_string() << "'.";
    }

    // initial bk_display(s) to manage
    for (auto it = p->cfg->bk_displays.begin(); it != p->cfg->bk_displays.end(); ++it) {
        p->add_and_connect_bk_display(it->first.c_str(), it->second);
    }

    // the main loop
    {
        mutex mtx;
        unique_lock<mutex> lck(mtx);
        vector<shared_ptr<EventLoop>> el_has_events;
        Priv::ExtCommands ext_cmd;
        while (true) {
            // we don't want to stick around
            if (!p->eventloop_instances.size()) {
                // wait for termination handler to stop (if any)
                while (sighandlers::is_sighandler_running()) {
                    usleep(5000);
                }
                puts(_("No more X server(s) to redirect. Exiting."));
                break;
            }
            // be patient
            p->cv_be_loop->wait(lck, [this, &el_has_events, &ext_cmd] {
                if (!p->eventloop_instances.size()) {
                    // bye for now! please start a new instance to manage
                    // another display!
                    return false;
                }
                bool exit_wait = false;
                // does one of the eventloop wakes me up?
                for (auto it = p->eventloop_instances.begin(); it != p->eventloop_instances.end(); ++it) {
                    if (it->second->has_queue.load(memory_order_acquire)) {
                        el_has_events.push_back(it->second);
                        exit_wait = true;
                    }
                }
                // any external command?
                ext_cmd = p->ext_caller.cmd.load(memory_order_acquire);
                exit_wait = exit_wait || (ext_cmd != Priv::ExtCommands::NONE);
                return exit_wait;
            });
            // let's handle those eventloop first! ...
            if (el_has_events.size()) {
                for (auto it = el_has_events.begin(); it != el_has_events.end(); ++it) {
                    intermsg::eventloopbackend::Message msg;
                    bool el_stopped = false;
                    while ((*it)->msg_queue.try_dequeue(msg)) {
                        switch (msg.type) {
                            case intermsg::eventloopbackend::Type::STOPPED:
                                p->print_server_stop_cause(
                                    *((intermsg::eventloopbackend::StopMessage*)&msg), (*it)->display_string);
                                p->delete_bk_display((*it)->display_string);
                                el_stopped = true;
                                break;
                            default:
                                LOG(WARNING) << "Received an unknown eventloopbackend msg.";
                                break;
                        }
                        if (el_stopped) {
                            break;
                        }
                    }
                    // We have to notify next iteration (if the event sent doesn't cause a deletion of this instance),
                    // that we've handled the events. Caution! May cause race  condition! But at this moment, this event
                    // queue system is only used to tell main backend if an eventloop has stopped. A stopped eventloop
                    // is going to fire this once, and non stopped eventloop is guaranteed not to fire this event, so no
                    // race condition is possible here.
                    if (!el_stopped) {
                        (*it)->has_queue.store(false, memory_order_relaxed);
                    }
                }
                el_has_events.clear();
            }
            // ... and then the external command
            if (ext_cmd != Priv::ExtCommands::NONE) {
                switch (ext_cmd) {
                    case Priv::ExtCommands::ADD_BK:
                        p->handle_ext_add_bk_display();
                        break;
                    case Priv::ExtCommands::REMOVE_BK:
                        p->handle_ext_remove_bk_display();
                        break;
                    case Priv::ExtCommands::GET_BK_LIST:
                        p->handle_ext_get_bk_displays();
                        break;
                    case Priv::ExtCommands::GET_BK_INFO:
                        p->handle_ext_get_bk_info();
                        break;
                    case Priv::ExtCommands::GET_MANAGED_WINDOWS:
                        p->handle_ext_get_managed_windows();
                        break;
                    case Priv::ExtCommands::RESCALE_BK:
                        p->handle_ext_rescale_bk();
                        break;
                    default:
                        LOG(WARNING) << "Unsupported external command";
                        break;
                }
                // we're done handling the external command.
                p->ext_caller.cmd.store(Priv::ExtCommands::NONE, memory_order_relaxed);
                p->ext_caller.ret_finished.store(true, memory_order_release);
            }
        }
    }
}

bool Backend::request_add_display(const string& display, BkFrontEndOptions options) {
    // deserialize 'options' to string. refer to BkFrontEndOptions for ordering.
    p->call_extfns(Priv::ExtCommands::ADD_BK,
        {display, to_string((int)options.scale_algorithm), to_string((int)options.framerate_mode),
            to_string((int)options.scale_factor)});
    return p->ext_caller.ret_status.load(memory_order_relaxed);
}

bool Backend::request_remove_bk_display(const std::string& display) {
    p->call_extfns(Priv::ExtCommands::REMOVE_BK, {display});
    bool ret = p->ext_caller.ret_status.load(memory_order_relaxed);
    if (ret) {
        printf("Display '%s' successfully disconnected and removed.", display.c_str());
    } else {
        printf("Display '%s' doesn't exist.", display.c_str());
    }
    putchar('\n');
    return ret;
}

bool Backend::request_rescale_bk_display(const std::string& display, int scfactor) {
    p->call_extfns(Priv::ExtCommands::RESCALE_BK, {display, to_string(scfactor)});
    return p->ext_caller.ret_status.load(memory_order_relaxed);
}

pair<bool, pxwm::states::BkFrontEndOptions> Backend::get_bk_display_info(const std::string& display) {
    p->call_extfns(Priv::ExtCommands::GET_BK_INFO, {display});
    if (p->ext_caller.ret_status.load(memory_order_relaxed)) {
        return {true, p->ext_caller.ret_bk_info};
    }
    return {false, {}};
}

const std::vector<std::string> Backend::get_managed_bk_displays() {
    p->call_extfns(Priv::ExtCommands::GET_BK_LIST, {});
    return p->ext_caller.ret_bk_displays;
}

const pair<bool, vector<pair<xcb_window_t, xcb_window_t>>> Backend::get_managed_windows(const std::string& display) {
    p->call_extfns(Priv::ExtCommands::GET_MANAGED_WINDOWS, {display});
    if (p->ext_caller.ret_status.load(memory_order_relaxed)) {
        return {true, p->ext_caller.ret_managed_windows_list};
    }
    return {false, {}};
}

bool Backend::Priv::add_and_connect_bk_display(const string& display, BkFrontEndOptions options) {
    // must be called from main thread
    assert(syscall(SYS_gettid) == getpid());
    // make sure the display isn't already connected
    auto bkdpy_comp = xutil::XDisplayComponent(display.c_str());
    if (bkdpy_comp.is_invalid()) {
        puts(_("Invalid backend display specification."));
        LOG(INFO) << "Invalid backend display specification.";
        return false;
    }
    if (!eventloop_instances.count(bkdpy_comp)) {
        // also make sure that we don't attempt to manage frontend display
        if (fe_dpy_component == bkdpy_comp) {
            puts(_("Cannot manage frontend display."));
            LOG(INFO) << "An attempt to manage frontend display was made.";
            return false;
        }
        xcb_connection_t* conn;
        xcb_screen_t* screen;
        shared_ptr<x11::VisualMap> visualmap;

        if (connect_bk_display(display, conn, screen, visualmap)) {
            std::map<xcb_extension_t*, x11::XExtensionInfo> ext_infos;
            // to "double check"! and retreive their 'firsts' code.
            if (x11::query_extensions(conn, {&xcb_damage_id, &xcb_xkb_id, &xcb_xfixes_id, &xcb_shape_id}, &ext_infos)) {
                // info about this managed display.
                printf(_("Requested backend X server '%1$s' successfully connected."), display.c_str());
                putchar('\n');
                printf(_("Scaling factor for '%1$s' is %2$dx."), display.c_str(), options.scale_factor);
                putchar('\n');
                LOG(INFO) << string_printf("Connected to backend display '%s', with the following options:\n"
                                           "Scale Algorithm: %s\n"
                                           "Scale Factor: %d\n"
                                           "Frame Rate: %s",
                    bkdpy_comp.to_string().c_str(),
                    Consts::values.vmap_scale_algorithms.at(options.scale_algorithm).c_str(), options.scale_factor,
                    Consts::values.vmap_framerate_modes.at(options.framerate_mode).c_str());
                // initialize/reset the backend (e.g. focus reset)
                reset_backend(conn, screen);
                // create an event loop and add it to the list (one liner!)
                EventLoop& evloop = *(
                    eventloop_instances[bkdpy_comp] =
                        make_unique<EventLoop>(conn, screen, display, ext_infos, visualmap,
                            options)).get();
                // go!
                auto existing_windows = query_existing_backend_windows(conn, screen, visualmap.get());
                evloop.start_run_loop(cv_be_loop, existing_windows);
                return true;
            } else {
                x11::disconnect_display(conn);
                LOG(INFO) << "Required extension isn't present on backend display " << display << ".";
            }
        } else {
            x11::disconnect_display(conn);
            LOG(INFO) << "Cannot connect to display " << display << ".";
        }
    } else {
        printf(_("Requested backend X server '%1$s' is already connected."), display.c_str());
        puts("");
        return true;
    }
    return false;
}

bool Backend::Priv::connect_bk_display(const string& display,
    xcb_connection_t*& conn,
    xcb_screen_t*& screen,
    shared_ptr<x11::VisualMap>& visualmap) {
    if (x11::connect_and_check_display(display, visualmap, true, conn, screen)) {
        // make sure required extension is available on the backend server
        std::set<xcb_extension_t*> req_exts = {
            &xcb_composite_id, &xcb_damage_id, &xcb_test_id, &xcb_xkb_id, &xcb_xfixes_id, &xcb_shape_id};
        if (!x11::query_extensions(conn, req_exts, nullptr)) {
            puts(_("Backend doesn't support all required extensions."));
            LOG(INFO) << "Backend doesn't support all required extensions.";
            return false;
        }
        // also verify that all required extensions' version is compatible
        if (!x11::init_and_check_extensions_version(conn, req_exts)) {
            puts(_("One or more required extensions in backend has unsupported version."));
            LOG(INFO) << "One or more required extensions in backend is incompatible.";
            return false;
        }
        // Redirect all windows that has been created/mapped on this display before this compositor's connection.
        // Also check whether another compositor is already running on this display.
        if (!init_composite_redir(conn, screen)) {
            return false;
        }
        // Sometimes backend X server unintentionally get some form of input, which triggers XKB NKN and reset it's
        // mapping. We are anticipating for that by selecting the mentioned XKB event.
        if (!select_backend_xkb_events(conn)) {
            puts(_("Cannot select XKB events on backend."));
            LOG(ERROR) << "Cannot select XKB events on backend.";
            return false;
        }
        // set backend default cursor to theme
        set_backend_default_cursor(conn, screen);
        // monitor cursor changes on backend window
        if (!select_backend_xfixes_cursor(conn, screen)) {
            puts(_("Cannot select XFIXES cursor change event on backend."));
            LOG(ERROR) << "Cannot select XFIXES cursor change event on backend.";
            return false;
        }
    } else {
        return false;
    }
    return true;
}

bool Backend::Priv::delete_bk_display(string display_string) {
    // must be called from main thread
    assert(syscall(SYS_gettid) == getpid());
    auto it = eventloop_instances.find(xutil::XDisplayComponent(display_string.c_str()));
    if (it != eventloop_instances.end()) {
        eventloop_instances.erase(it);
        return true;
    } else {
        LOG(INFO) << "Attempt to delete nonexistent display";
    }
    return false;
}

bool Backend::Priv::init_composite_redir(xcb_connection_t* conn, xcb_screen_t* screen) {
    // check for existing window manager
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    uint32_t select_input_val =
        XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT | XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY | XCB_EVENT_MASK_RESIZE_REDIRECT;
    auto select_req = xcb_change_window_attributes_checked(conn, screen->root, XCB_CW_EVENT_MASK, &select_input_val);
    err.pop() = xcb_request_check(conn, select_req);
    if (err.get() != nullptr) {
        fputsn(_("Another compositing window manager is already running on this display."), stderr);
        LOG(ERROR) << "Another window manager is detected on backend.";
        return false;
    }

    auto composite_req =
        xcb_composite_redirect_subwindows_checked(conn, screen->root, XCB_COMPOSITE_REDIRECT_AUTOMATIC);
    err.pop() = xcb_request_check(conn, composite_req);
    if (err.get() != nullptr) {
        fputsn(_("Cannot perform compositing redirection to this X server."), stderr);
        LOG(ERROR) << "Composite redirection failed.";
        return false;
    }

    return true;
}

bool Backend::Priv::select_backend_xkb_events(xcb_connection_t* conn) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    uint16_t enabled_events = XCB_XKB_EVENT_TYPE_NEW_KEYBOARD_NOTIFY;
    err.pop() = xcb_request_check(conn, xcb_xkb_select_events_aux_checked(conn, XCB_XKB_ID_USE_CORE_KBD, enabled_events,
                                            0, enabled_events, 0, 0, nullptr));
    if (err.get()) {
        LOG(ERROR) << string_printf(
            "Cannot select XKB events on backend (%s)", xcb_event_get_error_label(err.get()->error_code));
        return false;
    }
    return true;
}

void Backend::Priv::set_backend_default_cursor(xcb_connection_t* conn, xcb_screen_t* screen) {
    // xcursor context (to load the default cursor)
    xcb_cursor_context_t* xcursor_ctx;
    xcb_cursor_context_new(conn, screen, &xcursor_ctx);
    // no need to free this cursor
    auto cur = xcb_cursor_load_cursor(xcursor_ctx, "left_ptr");
    xcb_cursor_context_free(xcursor_ctx);
    // set to backend
    uint32_t cw_flag = XCB_CW_CURSOR;
    uint32_t cw_vals[] = {cur};
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    err.pop() = xcb_request_check(conn, xcb_change_window_attributes_checked(conn, screen->root, cw_flag, cw_vals));
    if (err.get()) {
        LOG(WARNING) << "Cannot set backend's root cursor: " << xcb_event_get_error_label(err.get()->error_code);
    }
}

bool Backend::Priv::select_backend_xfixes_cursor(xcb_connection_t* conn, xcb_screen_t* screen) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    err.pop() = xcb_request_check(
        conn, xcb_xfixes_select_cursor_input_checked(conn, screen->root, XCB_XFIXES_CURSOR_NOTIFY_MASK_DISPLAY_CURSOR));
    if (err.get()) {
        fputs(_("Cannot select xfixes for cursor change."), stdout);
        putchar(' ');
        puts(stringconsts::values().WARN_POINTER_FWD);
        return false;
    }
    // otherwise,
    return true;
}

vector<xcb_window_t> Backend::Priv::query_existing_backend_windows(xcb_connection_t* conn,
    xcb_screen_t* screen,
    x11::VisualMap* visualmap) {
    auto qt_cookie = xcb_query_tree(conn, screen->root);
    auto qt_reply = make_unique_malloc<xcb_query_tree_reply_t>(xcb_query_tree_reply(conn, qt_cookie, nullptr));
    auto child_count = xcb_query_tree_children_length(qt_reply.get());
    xcb_window_t* children = xcb_query_tree_children(qt_reply.get());

    vector<xcb_window_t> ret;
    while (child_count--) {
        if (common::accept_window_for_redir(conn, *children, visualmap)) {
            ret.push_back(*children);
        }
        ++children;
    }
    return ret;
}

void Backend::Priv::reset_backend(xcb_connection_t* conn, xcb_screen_t* scr) {
    // reset focus ...
    xcb_set_input_focus(conn, XCB_INPUT_FOCUS_NONE, XCB_INPUT_FOCUS_NONE, XCB_CURRENT_TIME);
    // ... and the associated EWMH property
    xcb_window_t none_window = XCB_WINDOW_NONE;
    xcb_change_property(conn, XCB_PROP_MODE_REPLACE, scr->root, x11::get_atom(conn, false, "_NET_ACTIVE_WINDOW"),
        XCB_ATOM_WINDOW, 32, 1, &none_window);
    xcb_flush(conn);
}

void Backend::Priv::call_extfns(ExtCommands cmd, initializer_list<string> params) {
    // we only able to handle 1 request at a time, so please queue!
    static mutex mtx;
    unique_lock<mutex> lock(mtx);
    // reset
    ext_caller.ret_finished.store(false, memory_order_relaxed);
    // cmd
    ext_caller.cmd_params.clear();
    for (auto param : params) {
        ext_caller.cmd_params.push_back(param);
    }
    ext_caller.cmd.store(cmd, memory_order_release);
    //"call"
    cv_be_loop->notify_one();
    // ret
    do {
        usleep(10000);
    } while (!ext_caller.ret_finished.load(memory_order_acquire));
}

void Backend::Priv::handle_ext_add_bk_display() {
    BkFrontEndOptions options{(ScalingAlgorithm)stoi(ext_caller.cmd_params[1]),
        (FrameRateMode)stoi(ext_caller.cmd_params[2]), SHMMode::PIXMAP, (uint16_t)stoi(ext_caller.cmd_params[3])};
    ext_caller.ret_status.store(add_and_connect_bk_display(ext_caller.cmd_params[0], options), memory_order_relaxed);
}

void Backend::Priv::handle_ext_remove_bk_display() {
    ext_caller.ret_status.store(delete_bk_display(ext_caller.cmd_params[0]), memory_order_relaxed);
}

void Backend::Priv::handle_ext_get_bk_displays() {
    ext_caller.ret_bk_displays.clear();
    for (auto it = eventloop_instances.begin(); it != eventloop_instances.end(); ++it) {
        ext_caller.ret_bk_displays.push_back(it->first.to_string());
    }
    ext_caller.ret_status.store(true, memory_order_relaxed);
}

void Backend::Priv::handle_ext_get_bk_info() {
    xutil::XDisplayComponent req(ext_caller.cmd_params[0].c_str());
    eventloop_instance_map_t::iterator it;
    if (check_and_get_mapped_value(eventloop_instances, req, it)) {
        ext_caller.ret_bk_info = it->second->get_applied_options();
        ext_caller.ret_status.store(true, memory_order_relaxed);
    } else {
        ext_caller.ret_status.store(false, memory_order_relaxed);
    }
}

void Backend::Priv::handle_ext_get_managed_windows() {
    xutil::XDisplayComponent req(ext_caller.cmd_params[0].c_str());
    eventloop_instance_map_t::iterator it;
    if (check_and_get_mapped_value(eventloop_instances, req, it)) {
        ext_caller.ret_managed_windows_list = it->second->get_managed_windows();
        ext_caller.ret_status.store(true, memory_order_relaxed);
    } else {
        ext_caller.ret_status.store(false, memory_order_relaxed);
    }
}

void Backend::Priv::handle_ext_rescale_bk() {
    xutil::XDisplayComponent req(ext_caller.cmd_params[0].c_str());
    eventloop_instance_map_t::iterator it;
    if (check_and_get_mapped_value(eventloop_instances, req, it)) {
        it->second->request_rescale(strtol(ext_caller.cmd_params[1].c_str(), nullptr, 10));
        ext_caller.ret_status.store(true, memory_order_relaxed);
    } else {
        ext_caller.ret_status.store(false, memory_order_relaxed);
    }
}

void Backend::Priv::print_server_stop_cause(intermsg::eventloopbackend::StopMessage msg, string display) {
    if (msg.stop_cause != intermsg::eventloopbackend::StopCause::REQ_STOP) {
        fprintf(stderr, _("Disconnected from backend X Server '%1$s'. Reason: "), display.c_str());
        switch (msg.stop_cause) {
            case intermsg::eventloopbackend::StopCause::SERVER_DISCONNECT:
                fputsn(_("Backend X Server has gone away."), stderr);
                break;
            case intermsg::eventloopbackend::StopCause::FRONTEND_SERVER_DISCONNECT:
                fputsn(_("Frontend X Server has gone away."), stderr);
                break;
            case intermsg::eventloopbackend::StopCause::FRONTEND_SERVER_CANNOT_CONNECT:
                fputsn(_("Cannot connect to the specified frontend X server."), stderr);
                break;
            case intermsg::eventloopbackend::StopCause::FRONTEND_UNSUPPORTED_EXT:
                fputsn(_("Frontend doesn't have required extensions."), stderr);
                break;
            case intermsg::eventloopbackend::StopCause::FRONTEND_INIT_ERROR:
                fputsn(_("Frontend's initialisation error. Check log for more details."), stderr);
                break;
            case intermsg::eventloopbackend::StopCause::FRONTEND_VISUAL_UNSUPPORTED:
                fputsn(_("Frontend doesn't have supported pixmap format or visual."), stderr);
                break;
            case intermsg::eventloopbackend::StopCause::XKB_INIT_ERR:
                fputsn(_("Failed to setup XKB."), stderr);
                break;
            case intermsg::eventloopbackend::StopCause::UNKNOWN:
            default:
                LOG(ERROR) << "Unknown fatal error on event looper.";
                fputsn(_("Unknown fatal error on event looper."), stderr);
                break;
        }
        putchar('\n');
    }
}
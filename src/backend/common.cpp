#include "backend/common.h"
#include "common/x11.h"
#include "shared/memory.h"
#include <cstdio>
#include <cstring>
#include <glog/logging.h>
#include <set>
#include <xcb/damage.h>
#include <xcb/shape.h>
#include <xcb/xcb.h>
#include <xcb/xcb_util.h>

using namespace px::memory;
using namespace pxwm::backend::common;
using namespace pxwm::common::x11;
using namespace std;

#define BACKEND_COMMON_DEBUG

bool pxwm::backend::common::accept_window_for_redir(xcb_connection_t* conn, xcb_window_t window, VisualMap* visualmap) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;

    // retreive attribute
    auto attr =
        make_unique_malloc(xcb_get_window_attributes_reply(conn, xcb_get_window_attributes(conn, window), &err.pop()));
    // dunno why, but skip if the window doesn't allow us to access its attr
    if (err.get() != nullptr) {
        return false;
    }

    // ensure we can process it's visual
    auto& visual = visualmap->get_visual_type(attr->visual);
    if (!is_visual_supported(visual)) {
        return false;
    }
    // ensure the pixmap format is compatible
    if (!visualmap->get_pixmap_depth_bpp_format(visual.depth).count(32) ||
        !visualmap->get_pixmap_scanline_pad(visual.depth).count(32)) {
        return false;
    }

    // these attributes check ensure that window is visible
    if (attr->_class == XCB_WINDOW_CLASS_INPUT_OUTPUT && attr->map_state == XCB_MAP_STATE_VIEWABLE) {
        return true;
    }
    return false;
}

void pxwm::backend::common::add_window_default_event_listeners(xcb_connection_t* conn, xcb_window_t window) {
    // we listen to the window's property change events.
    // note that this only apply to current X connections,
    // e.g. this won't alter original app's event listener.

    // listen for property change (name,config)
    uint32_t select_event_val = XCB_EVENT_MASK_PROPERTY_CHANGE | XCB_EVENT_MASK_FOCUS_CHANGE;
    xcb_change_window_attributes(conn, window, XCB_CW_EVENT_MASK, &select_event_val);
    // select extensions' events
    xcb_shape_select_input(conn, window, 1);
    xcb_flush(conn);
}

void pxwm::backend::common::create_damage_listener(xcb_connection_t* conn,
    xcb_window_t window,
    ManagedWindowData& data) {
    // listen to damage event
    data.damage_id = xcb_generate_id(conn);
    xcb_damage_create(conn, data.damage_id, window, XCB_DAMAGE_REPORT_LEVEL_RAW_RECTANGLES);
    xcb_flush(conn);
}

void pxwm::backend::common::remove_window_default_event_listener(xcb_connection_t* conn,
    xcb_window_t window,
    ManagedWindowData& data) {
    // ideally, refer to what 'add_window_default_event_listeners' has done + delete damage listener

    // property changes, etc.
    uint32_t select_event_val = 0;
    xcb_change_window_attributes(conn, window, XCB_CW_EVENT_MASK, &select_event_val);
    // damage
    xcb_damage_destroy(conn, data.damage_id);
}